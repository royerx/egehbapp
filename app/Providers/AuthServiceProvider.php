<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Security\Role' => 'App\Policies\Security\RolePolicy',
        'App\Models\Security\Permission' => 'App\Policies\Security\PermissionPolicy',
        'App\Models\Security\User' => 'App\Policies\Security\UserPolicy',
        'App\Models\Management\Student' => 'App\Policies\Management\StudentPolicy',
        'App\Models\Management\Teacher' => 'App\Policies\Management\TeacherPolicy',
        'App\Models\Management\Administrative' => 'App\Policies\Management\AdministrativePolicy',
        'App\Models\Management\Period' => 'App\Policies\Management\PeriodPolicy',
        'App\Models\Management\Lesson' => 'App\Policies\Management\LessonPolicy',
        'App\Models\Election\Position' => 'App\Policies\Election\PositionPolicy',
        'App\Models\Election\Group' => 'App\Policies\Election\GroupPolicy',
        'App\Models\Election\Candidate' => 'App\Policies\Election\CandidatePolicy',
        'App\Models\System\Parameter' => 'App\Policies\System\ParameterPolicy',
        'App\Models\Resource\Device' => 'App\Policies\Resource\DevicePolicy',
        'App\Models\Resource\Application' => 'App\Policies\Resource\ApplicationPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
