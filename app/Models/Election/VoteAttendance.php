<?php

namespace App\Models\Election;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Student;
use App\Models\Management\Period;
use Carbon\Carbon;

class VoteAttendance extends Model
{
    use HasFactory;

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'place',
        'state',
        'student_id',
        'period_id',
    ];

    #RELACIONES

    /**
     * Relacion de Asistencia de Votacion con Estudiante
     *
     * @return collection \App\Models\Management\Student
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Relacion de Asistencia de Votacion con Periodo
     *
     * @return collection \App\Models\Management\Period
     */
    public function period()
    {
        return $this->belongsTo(Period::class);
    }

    #ALMACENAMIENTO
    /**
     * Guardar Asistencia de Votación
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Election\VoteAttendance
     */
    public function store($request)
     {
        $student=Student::where('dni',$request->dni)->first();
        $year = Carbon::now()->format('Y');
        $period=Period::where('year',$year)->first();
        $voteAttendance=VoteAttendance::where('student_id',$student->id)->where('period_id',$period->id)->where('state','Voto')->first();
        if(!$voteAttendance){
            $voteAttendance = self::create(array_merge($request->all(),[
                'student_id' => $student->id,
                'period_id' => $period->id,
            ]));
            return $voteAttendance;
        }else{
            return null;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
