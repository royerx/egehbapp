<?php

namespace App\Models\Election;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Election\Group;
use App\Models\Management\Period;
use App\Models\Management\Student;
use Illuminate\Support\Facades\DB;

class Vote extends Model
{
    use HasFactory;

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'picture',
        'period_id',
        'group_id',
    ];


    #RELACIONES

    /**
     * Relacion de Voto con Agrupación
     *
     * @return collection \App\Models\Election\Group
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    #ALMACENAMIENTO
    /**
     * Guardar Asistencia de Votación
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Election\VoteAttendance
     */
    public function store($request)
    {
        if($request->group){
            $vote = self::create([
                'group_id' => $request->group['id'],
                'period_id' => Period::getCurrent()->id,
            ]);
        }
        else{
            $group=Group::getBlank();
            $vote = self::create([
                'group_id' => $group->id,
                'period_id' => Period::getCurrent()->id,
            ]);
        }
        
        return $vote;
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
    public static function result($period_id){
        $quantity = Vote::where("period_id","$period_id")->count();
            
            $students = Student::whereHas('lessons', function($q) use($period_id){
                            $q->where('period_id', "$period_id");
                        })->count();

            $votes = DB::table('votes as V')
                        ->join('groups as G','V.group_id','=','G.id')
                        ->selectRaw("G.name,G.picture,
                                    CONCAT(ROUND(COUNT(V.id)/$quantity*100,2),'%') as percent,
                                    COUNT(V.id) as quantity")
                        ->groupBy("G.id","G.name","G.picture")
                        ->where('V.period_id',"$period_id")->get();
            $participation = round(($quantity/$students)*100,2);
            $absenteeism = 100-$participation;
            $result = [
                        "data" => $votes,
                        "quantity" => $quantity,
                        "total" => $students,
                        "participation" => $participation,
                        "absenteeism" => $absenteeism
                    ];
            return $result;
    }
}
