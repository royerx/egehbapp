<?php

namespace App\Models\Election;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Election\Candidate;
use App\Models\Election\Vote;
use App\Models\Management\Period;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class Group extends Model
{
    use HasFactory;

    const PICTURE_PATH = '/img/groups';
    const BLANK = 'Voto en Blanco';

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'picture',
        'period_id'
    ];

    #RELACIONES

    /**
     * Relacion de Agrupacion con Candidatos
     *
     * @return collection App\Models\Election\Candidate
     */
    public function candidates()
    {
        return $this->hasMany(Candidate::class);
    }
    
    /**
     * Relacion de Agrupacion con Votos
     *
     * @return collection \App\Models\Election\Vote
     */
    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    /**
     * Relacion de Agrupacion con Periodo
     *
     * @return collection App\Models\Election\Period
     */
    public function period()
    {
        return $this->belongsTo(Period::class);
    }

    #ALMACENAMIENTO
    /**
     * Guardar Agrupacion
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Election\Group
     */
    public function store($request)
     {
         try {
            
            DB::beginTransaction();
                
                $period= json_decode($request->period);
                $picture=$this->savePicture($request->photo);
                $group = self::create(array_merge($request->all(),[
                    'picture' => $picture,
                    'period_id' => $period->id,
                ]));

            DB::commit();
            return $group;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
     }
     
     /**
     * Modificar Agrupacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param \App\Models\Election\Group
     * @return \App\Models\Election\Group
     */
     public function my_update($request,$group)
     {
         $request['period_id']=$request->period['id'];
         return self::update($request->all());
     }
     
     /**
     * Eliminar Agrupacion
     *
     * @param \App\Models\Election\Group
     * @return \App\Models\Election\Group
     */
     public function remove(){
         try {
            
            DB::beginTransaction();
                $this->deletePicture($this->picture);
                $this->candidates()->delete();
                $this->delete();
            DB::commit();
            
            return $this;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->orWhere('name','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
    /**
     * Guardar foto
     *
     * @param  image $photo
     * @param  string $picture
     * @return string path guardado
     */
    public function savePicture($photo,$picture = NULL)
    {
        $this->deletePicture($picture);
        $extension= $photo->extension();
        $path=$photo->storeAs(self::PICTURE_PATH,time()."_".Str::random(20).".{$extension}",'public');
        return  '/'.$path;
    }
    
    /**
     * Eliminar foto
     *
     * @param  string $picture
     */
    public function deletePicture($picture){
        if($picture){
            Storage::disk('public')->delete("$picture");
        }
    }

    public static function getBlank(){
        $blank = Group::where('name',self::BLANK)->first();
        return $blank;
    }
}
