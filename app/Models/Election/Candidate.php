<?php

namespace App\Models\Election;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Student;
use App\Models\Election\Candidate;
use App\Models\Election\Position;

class Candidate extends Model
{
    use HasFactory;

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'position_id',
        'student_id',
        'group_id'
    ];

    #RELACIONES

    /**
     * Relacion de Candidato con Agrupacion
     *
     * @return collection \App\Models\Election\Candidate
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    
    /**
     * Relacion de Candidato con Estudiante
     *
     * @return collection \App\Models\Management\Student
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Relacion de Candidato con Cargo
     *
     * @return collection \App\Models\Election\Position
     */
    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    #ALMACENAMIENTO

    /**
     * Guardar Agrupacion
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Election\Group
     */
    public function store($request)
     {
        $candidate = self::create([
            'position_id' => $request->position['id'],
            'student_id' => $request->student['id'],
            'group_id' => $request->group['id'],
        ]);

        return $candidate;
     }
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
