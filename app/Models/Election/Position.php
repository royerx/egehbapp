<?php

namespace App\Models\Election;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Election\Candidate;

class Position extends Model
{
    use HasFactory;

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    #RELACIONES

    /**
     * Relacion de Cargo con Candidatos
     *
     * @return collection \App\Models\Election\Candidate
     */
    public function candidates()
    {
        return $this->hasMany(Candidate::class);
    }

    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->orWhere('name','LIKE',"%$find%");
    }
    #OTRAS OPERACIONES
}
