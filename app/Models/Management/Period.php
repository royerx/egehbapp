<?php

namespace App\Models\Management;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\Election\Group;
use App\Models\Management\Lesson;
use App\Models\Management\Student;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Period extends Model
{
    use HasFactory;

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'year',
        'active',
        'description'
    ];

    #RELACIONES

    /**
     * Relacion de Periodo con Agrupacion  
     *
     * @return collection \App\Models\Election\Group
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    /**
     * Relacion de Periodo con Grado y seccion  
     *
     * @return collection \App\Models\Management\Lesson
     */
    public function lessons()
    {
        return $this->hasMany(Leson::class);
    } 

    /**
     * Relacion de Periodos con estudiantes
     *
     * @return collection \App\Models\Management\Lesson
     */
    public function students()
    {
        return $this->belongsToMany(Student::class,'lessons')->withPivot('id','grade','section');
    }

    /**
     * Relacion de Periodos con estudiantes
     *
     * @return collection \App\Models\Management\Attendance
     */
    public function voteAttendances()
    {
        return $this->hasMany(VoteAttendance::class);
    }
    #ALMACENAMIENTO
    /**
     * Guardar Periodo
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Management\Period
     */
    public function store($request)
     {
         try {
            
            DB::beginTransaction();
                $this->setInactive();
                $period = self::create(array_merge($request->all(),[
                    'active' => true
                ]));

            DB::commit();
            return $period;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
     }

    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->orWhere('name','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
    
    public static function getCurrent()
    {
        return Period::where('active',true)->get()[0];
    }

    public function setInactive(){
        Period::where('active', true)->update(['active' => false]);
    }
}
