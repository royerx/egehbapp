<?php

namespace App\Models\Management;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Models\Security\User;
use App\Models\API\Attendance;
use App\Models\API\Incident;

class Person extends Model
{
    use HasFactory;

    const STUDENT = 'Estudiante';
    const TEACHER = 'Profesor';
    const ADMINISTRATIVE = 'Administrativo';
    
    protected $defaultPicture='';

    protected $table = 'people';
    
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'lastName',
        'firstName',
        'dni',
        'birthday',
        'gender',
        'address',
        'phone',
        'picture',
        'email'
    ];

    /**
     * Atributos calculados o generados
     *
     * @var array
     */
    protected $appends=['fullName'];
    
    #RELACIONES

    /**
     * Relacion de Persona con Usuario  
     *
     * @return collection \App\Models\Security\User
     */
    public function user()
    {
        return $this->hasOne(User::class);
    } 

    /**
     * Relacion de Usuario con Asistencia  
     *
     * @return collection \App\Models\API\Attendance
     */
    public function attendances()
    {
        return $this->hasMany(Attendance::class);
    } 

    /**
     * Relacion de Usuario con Incidente  
     *
     * @return collection \App\Models\API\Incident
     */
    public function incidents()
    {
        return $this->hasMany(Incident::class);
    } 
    
    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /**
     * Obtener el nombreCompleto
     *
     * @return string path
     */
    public function getFullNameAttribute()
    {
        if($this->lastName){
            return $this->firstName.' '.$this->lastName;
        }else{
            return $this->firstName;
        }
        
    }

    /* Query Scopes*/
    public function scopeFirsName($query,$find)
    {
        if($find)
            return $query->orWhere('firstName','LIKE',"%$find%");
    }

    public function scopeLastName($query,$find)
    {
        if($find)
            return $query->orWhere('lastName','LIKE',"%$find%");
    }
    public function scopeDni($query,$find)
    {
        if($find)
            return $query->orWhere('dni','LIKE',"%$find%");
    }
    public function scopeEmail($query,$find)
    {
        if($find)
            return $query->orWhere('email','LIKE',"%$find%");
    }

    public function scopeFindForUser($query,$find)
    {
        if($find)
            return $query->orWhere('firstName','LIKE',"%$find%")->orWhere('lastName','LIKE',"%$find%")->orWhere('dni','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES

    /**
     * Guardar foto
     *
     * @param  image $photo
     * @param  string $picture
     * @return string path guardado
     */
    public function savePicture($photo,$path,$picture = NULL)
    {
        $this->deletePicture($picture);
        $extension= $photo->extension();
        return '/'.$photo->storeAs($path,time()."_".Str::random(20).".{$extension}",'public');
    }
    
    /**
     * Eliminar foto
     *
     * @param  string $picture
     */
    public function deletePicture($picture){
        if($picture && $picture!=$this->defaultPicture){
            Storage::disk('public')->delete("$picture");
        }
    }
}
