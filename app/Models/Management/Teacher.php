<?php

namespace App\Models\Management;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Resource\Application;
class Teacher extends Person
{
    use HasFactory;

    const PICTURE_PATH = '/img/teachers';
    
    protected $defaultPicture = '/img/teachers/teacher.png';

    #RELACIONES

    /**
     * Relacion de Docente y solicitud
     *
     * @return collection \App\Models\Resource\Application
     */
    public function Applications()
    {
        return $this->hasMany(Application::class);
    }

    #ALMACENAMIENTO
    /**
     * Guardar Profesor
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Management\Teacher
     */
    public function store($request)
     {
         try {
            
            DB::beginTransaction();
                $picture=($request->photo)?$this->savePicture($request->photo,self::PICTURE_PATH):$this->defaultPicture;
                $teacher = self::create(array_merge($request->all(),[
                    'picture' => $picture
                ]));

            DB::commit();
            return $teacher;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
     }
     
     /**
     * Eliminar Profesor
     *
     * @param \App\Models\Management\Teacher
     * @return \App\Models\Management\Teacher
     */
     public function remove($teacher){
        try {
            
            DB::beginTransaction();
                $this->deletePicture($teacher->picture);
                $teacher = $teacher->delete();
            DB::commit();
            
            return $teacher;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    public function scopeGetByRole($query)
    {
        return $query->where('type',self::TEACHER);
    }

    #OTRAS OPERACIONES
}
