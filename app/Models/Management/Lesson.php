<?php

namespace App\Models\Management;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Student;
use App\Models\Management\Period;

class Lesson extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'grade',
        'section',
        'student_id',
        'period_id',
    ];

    #RELACIONES

    /**
     * Relacion de Grado y Seccion con Estudiante  
     *
     * @return collection \App\Models\Management\Student
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Relacion de Grado y Seccion con Periodo  
     *
     * @return collection \App\Models\Management\Period
     */
    public function period()
    {
        return $this->belongsTo(Period::class);
    } 

    #ALMACENAMIENTO

    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
