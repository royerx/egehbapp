<?php

namespace App\Models\Management;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Administrative extends Person
{
    use HasFactory;

    const PICTURE_PATH = '/img/administratives';
    
    protected $defaultPicture = '/img/administratives/administrative.png';

    #RELACIONES

    #ALMACENAMIENTO
    /**
     * Guardar Administrativo
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Management\Administrative
     */
    public function store($request)
     {
        try {
            
            DB::beginTransaction();
                $picture=($request->photo)?$this->savePicture($request->photo,self::PICTURE_PATH):$this->defaultPicture;
                $administrative = self::create(array_merge($request->all(),[
                    'picture' => $picture
                ]));

            DB::commit();
            return $administrative;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
     }
     
     /**
     * Eliminar Administrativo
     *
     * @param \App\Models\Management\Administrative
     * @return \App\Models\Management\Administrative
     */
     public function remove($administrative){
        try {
            
            DB::beginTransaction();
                $this->deletePicture($administrative->picture);
                $administrative = $administrative->delete();
            DB::commit();
            
            return $administrative;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    public function scopeGetByRole($query)
    {
        return $query->where('type',self::ADMINISTRATIVE);
    }

    #OTRAS OPERACIONES
}
