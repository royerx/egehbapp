<?php

namespace App\Models\Management;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Lesson;
use App\Models\Management\Period;
use App\Models\Election\Candidate;
use App\Models\Election\VoteAttendance;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Models\API\Attendance;
use App\Models\API\Incident;

class Student extends Person
{
    use HasFactory;

    const PICTURE_PATH = '/img/students';
    
    protected $defaultPicture = '/img/students/student.png';

    #RELACIONES
    /**
     * Relacion de Estudiante con Candidatos
     *
     * @return collection \App\Models\Election\Candidate
     */
    public function candidate()
    {
        return $this->hasMany(Candidate::class);
    }

    /**
     * Relacion de Estudiante con Asistencias de Votacion
     *
     * @return collection \App\Models\Election\VoteAttendance
     */
    public function voteAttendances()
    {
        return $this->hasMany(VoteAttendance::class);
    }

    /**
     * Relacion de Estudiante con Grado y Seccion
     *
     * @return collection \App\Models\Management\Lesson
     */
    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }

    /**
     * Relacion de Estudiante con periodos
     *
     * @return collection \App\Models\Management\Lesson
     */
    public function periods()
    {
        return $this->belongsToMany(Period::class,'lessons')->withPivot('id','grade','section');
    }

    /**
     * Relacion de Estudiante con Asistencia
     *
     * @return collection \App\Models\API\Attendance
     */
    public function attendances()
    {
        return $this->hasMany(Attendance::class);
    }

    /**
     * Relacion de Estudiante con Incidencia
     *
     * @return collection \App\Models\API\Incident
     */
    public function incidents()
    {
        return $this->hasMany(Incident::class);
    }

    #ALMACENAMIENTO
    
    /**
     * Guardar Estudiante
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Management\Student
     */
    public function store($request)
     {
         try {
            
            DB::beginTransaction();
                $picture=($request->photo)?$this->savePicture($request->photo,self::PICTURE_PATH):$this->defaultPicture;
                $student = self::create(array_merge($request->all(),[
                    'picture' => $picture
                ]));

            DB::commit();
            return $student;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
     }
     
     /**
     * Eliminar Estudiante
     *
     * @param \App\Models\Management\Student
     * @return \App\Models\Management\Student
     */
     public function remove($student){
        try {
            
            DB::beginTransaction();
                $this->deletePicture($student->picture);
                $student = $student->delete();
            DB::commit();
            
            return $student;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    public function scopeGetByRole($query)
    {
        return $query->where('type',self::STUDENT);
    }
    
    #OTRAS OPERACIONES

    public function generateQR(){
        QrCode::size(130)->generate('Hola','../public/img/assets/qrcode.svg');
    }
}
