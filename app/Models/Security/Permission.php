<?php

namespace App\Models\Security;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Security\PermissionAction;

class Permission extends Model
{
    use HasFactory;

    const EXCEPT_TABLES = [
        'role_user',
        'permission_role',
        'password_resets',
        'migrations',
        'failed_jobs'
    ];
    
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description'
    ];

     #RELACIONES

    /**
     * Relacion de Permiso con Acciones de Permiso
     *
     * @return collection \App\Models\Security\PermissionAction
     */
    public function permissionActions()
    {
        return $this->hasMany(PermissionAction::class);
    }

    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN

    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->Where('name','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
}
