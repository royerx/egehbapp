<?php

namespace App\Models\Security;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Models\Security\Role;
use App\Models\Security\Permission;
use App\Models\Management\Person;
use App\Mail\UserRegistered;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'password',
        'person_id'
    ];

    /**
     * Atributos que seran ocultos
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Atributos convertidos a tipos nativos
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    #RELACIONES
      
    /**
     * Relacion de Usuario con Rol  
     *
     * @return collection \App\Models\Security\Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Relacion de Usuario con Persona  
     *
     * @return collection \App\Models\Management\Person
     */
    public function person()
    {
        return $this->belongsTo(Person::class);
    } 

    #ALMACENAMIENTO
        
    /**
     * Guardar Usuario
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function store($request)
     {
        $password = bcrypt($request->password);
        $user = self::create(array_merge($request->all(),[
            'password' => $password,
            'person_id' => $request->person['id'],
        ]));
        return $user;
     }

     /**
     * Editar Usuario
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function my_update($request)
     {
        try {
            
            DB::beginTransaction();
                $person = Person::findOrFail($request->person['id']);
                $person->update($request->person);
                $this->update(['login'=>$request->login]);
            DB::commit();
            
            return $this;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
     
     /**
     * Eliminar Usuario
     *
     * @param \App\Models\Security\User
     * @return \App\Models\Security\User
     */
     public function remove($user){
        try {
            
            DB::beginTransaction();
                $user->roles()->detach();
                $user = $user->delete();
            DB::commit();
            
            return $user;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN

    /* Query Scopes*/
    public function scopePersonFind($query,$find)
    {
        if($find)
            return $query->WhereHas('person',
                                        function($query) use($find){
                                            $query->Where('firstName','LIKE',"%$find%")
                                                  ->orWhere('lastName','LIKE',"%$find%")
                                                  ->orWhere('dni','LIKE',"%$find%")
                                                  ->orWhere('email','LIKE',"%$find%");
                                                });
    }
    public function scopeLogin($query,$find)
    {
        if($find)
            return $query->orWhere('login','LIKE',"%$find%");
    }
    #OTRAS OPERACIONES

    /**
     * Verificar permiso
     *
     * @param  string $permission
     * @return boolean
     */
    public function hasPermission($permission)
    {
       
        $role = Role::whereId(session('role_id'))->first();
        if ($role->special=="all-access"){
            return true;
        }

        if ($role->special=="no-access"){
            return false;
        }

        foreach($role->permissionActions()->get() as $item){
            if($item->slug==$permission){
                return true;
            }
        }

        return false;

    }
    
    /**
     * Asignar rol
     *
     * @param  array \App\Models\Security\Role $roles
     * @return void
     */
    public function setRoleSession($roles){
        if($roles->count()==1){
            Session::put([
                'role_id' => $roles[0]->id,
            ]);
        }
    }
}
