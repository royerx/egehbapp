<?php

namespace App\Models\Security;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\Security\PermissionAction;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    use HasFactory;
        
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'special'
    ];
    #RELACIONES

    /**
     * Relacion de Rol con Usuario  
     *
     * @return collection \App\Models\Security\User
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Relacion de Rol con Acciones de Permiso
     *
     * @return collection \App\Models\Security\PermissionAction
     */
    public function permissionActions()
    {
        return $this->belongsToMany(PermissionAction::class);
    }

    #ALMACENAMIENTO

    /**
     * Guardar rol
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\Role
     */
    public function store($request)
     {
         return self::create($request->all());
     }
          
     /**
     * Modificar rol
     *
     * @param  \Illuminate\Http\Request $request
     * @param \App\Models\Security\Role
     * @return \App\Models\Security\Role
     */
     public function my_update($request)
     {
         return self::update($request->all());
     }
     /**
     * Eliminar Rol
     *
     * @param \App\Models\Security\Role
     * @return \App\Models\Security\Role
     */
     public function remove($role){
        try {
            
            DB::beginTransaction();
                $role->permissionActions()->detach();
                $role = $role->delete();
            DB::commit();
            
            return $role;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->Where('name','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
}
