<?php

namespace App\Models\Security;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Security\Permission;
use App\Models\Security\Role;

class PermissionAction extends Model
{
    use HasFactory;
    
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'action',
        'slug',
        'description'
    ];

     #RELACIONES

    /**
     * Relacion de Acciones de Permiso con Permiso
     *
     * @return collection \App\Models\Security\Role
     */
    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }

    /**
     * Relacion de Acciones de Permiso con Roles
     *
     * @return collection \App\Models\Security\Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
