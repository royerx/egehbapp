<?php

namespace App\Models\Resource;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Resource\Application;
use Illuminate\Support\Facades\DB;

class Device extends Model
{
    use HasFactory;

    const PICTURE_PATH = '/img/devices';
    
    CONST DEFAULT_PATH = '/img/devices/device.png';

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'code', 
        'name', 
        'brand', 
        'model', 
        'type', 
        'color', 
        'serie', 
        'state', 
        'date', 
        'amount', 
        'place', 
        'detail', 
        'picture'
    ];

    /**
     * Atributos calculados o generados
     *
     * @var array
     */
    protected $appends=['inventoried'];

    #RELACIONES

    /**
     * Relacion de Recurso y Solicitud 
     *
     * @return collection \App\Models\Resource\Application
     */
    public function Applications()
    {
        return $this->hasMany(Application::class);
    }
    
    #ALMACENAMIENTO
    /**
     * Guardar Recurso
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Resource\Device
     */
    public function store($request)
     {
        try {
            
            DB::beginTransaction();
                $picture=($request->photo)?$this->savePicture($request->photo,self::PICTURE_PATH):self::DEFAULT_PATH ;
                $device = self::create(array_merge($request->all(),[
                    'picture' => $picture
                ]));

            DB::commit();
            return $device;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
     }
     
     /**
     * Eliminar Device
     *
     * @param \App\Models\Resource\Device
     * @return \App\Models\Resource\Device
     */
     public function remove($device){
        try {
            
            DB::beginTransaction();
                $this->deletePicture($device->picture);
                $device = $device->delete();
            DB::commit();
            
            return $device;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /* Query Scopes*/
    public function scopeCode($query,$find)
    {
        if($find)
            return $query->orWhere('code','LIKE',"%$find%");
    }

    public function scopeName($query,$find)
    {
        if($find)
            return $query->orWhere('name','LIKE',"%$find%");
    }
    /**
     * Obtener el tipo
     *
     * @return string path
     */
    public function getInventoriedAttribute()
    {
        if($this->type=='no inventariado'){
            return false;
        }
        return true;
        
    }
    
    #OTRAS OPERACIONES
}
