<?php

namespace App\Models\Resource;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Teacher;
use App\Models\Resource\Device;

class Application extends Model
{
    use HasFactory;

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'date', 
        'attendace', 
        'delivery', 
        'reception',
        'quantity', 
        'grade',
        'section',
        'place',
        'detail',
        'teacher_id', 
        'device_id'
    ];

    /**
     * Atributos calculados o generados
     *
     * @var array
     */
    protected $appends=['state'];

    #RELACIONES

    /**
     * Relacion de Solicitud y Docente  
     *
     * @return collection \App\Models\Management\Teacher
     */
    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    /**
     * Relacion de Solicitud y Recurso  
     *
     * @return collection \App\Models\Resource\Device
     */
    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    #ALMACENAMIENTO
    /**
     * Guardar Solicitud
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Resource\Application
     */
    public function store($request)
     {
        $application = self::create(array_merge($request->all(),[
            'teacher_id' => $request->teacher['id'],
            'device_id' => $request->device['id'],
        ]));
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /**
     * Obtener el estado
     *
     * @return string path
     */
    public function getStateAttribute()
    {
        if($this->reception){
            return 'Devuelto';
        }else if($this->delivery){
            return 'Recibido';
        }else if($this->date){
            return 'Registrado';
        }else{
            return '';
        }
        
    }

    /* Query Scopes*/
    public function scopeTeacher($query,$find)
    {
        if($find)
            return $query->WhereHas('teacher',function($query) use($find){$query->Where('firstName','LIKE',"%$find%")->orWhere('lastName','LIKE',"%$find%")->orWhere('dni','LIKE',"%$find%");});
    }
    #OTRAS OPERACIONES
}
