<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'value',
        'beginDate',
        'endDate',
        'description'
    ];

    const ENTRY_TIME = 'Hora de Entrada';
    const LATE_TOLERANCE = 'Toleracia Tardanza';
    const ABSENCE_TOLERANCE = 'Tolerancia Falta';

    #RELACIONES

    #ALMACENAMIENTO

    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->Where('name','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
}
