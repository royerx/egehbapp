<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Student;
use App\Models\Management\Person;
use App\Models\Security\User;

class Incident extends Model
{
    use HasFactory;
    
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'time',
        'type',
        'detail'
    ];

    #RELACIONES

    /**
     * Relacion de Incidente con Estudiante
     *
     * @return collection \App\Models\Management\Student
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Relacion de Incidente con Persona
     *
     * @return collection \App\Models\Security\User
     */
    public function person()
    {
        return $this->belongsTo(Person::class,'user_id');
    }

    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
