<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Person;
use App\Models\Management\Student;

class Attendance extends Model
{
    use HasFactory;

    const ATTENDANCE = 'Asistencia';
    const LATE = 'Tarde';
    const ABSENCE = 'Falta';
    const ABSENCE_EXCUSE = 'Justificación';
    const LATE_EXCUSE = 'Justificación Tardanza';
    
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'time',
        'type',
        'detail'
    ];
    /**
     * Atributos calculados o generados
     *
     * @var array
     */
    protected $appends=['record'];

    #RELACIONES

    /**
     * Relacion de Asistencia con Estudiante
     *
     * @return collection \App\Models\Management\Student
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Relacion de Asistencia con Persona
     *
     * @return collection \App\Models\Security\Uer
     */
    public function person()
    {
        return $this->belongsTo(Person::class,'user_id');
    }

    #ALMACENAMIENTO

    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /**
     * Obtener el tipo
     *
     * @return string path
     */
    public function getRecordAttribute()
    {
        if($this->type==$this::ATTENDANCE){
            return "✔";
        }elseif($this->type==$this::LATE){
            return "T";
        }elseif($this->type==$this::ABSENCE){
            return "F";
        }elseif($this->type==$this::ABSENCE_EXCUSE){
            return "J";
        }elseif($this->type==$this::LATE_EXCUSE){
            return "U";
        }
        return ' ';
        
    }
    
    #OTRAS OPERACIONES
}
