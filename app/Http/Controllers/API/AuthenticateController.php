<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Security\User;
use App\Models\Management\Student;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthenticateController extends Controller
{
    //use AuthenticatesUsers;
    /**
     * Asignar el atributo para el inicio de sesion
     *
     * @return string login: Atributo para iniciar sesion
     */    

    /*public function username(){
        return 'login';
    }*/

    /**
     * Iniciar sesion y generar token
     *
     * @return json token de inicio de sesion
     */    

    public function login(Request $request){
        if (!Auth::attempt($request->only('login', 'password'))) {
            return response()->json(["message"=>"Sin autorización"]);
        }
        
        $user = User::where('login', $request->login)->first();
        $user->tokens()->delete();

            /*if (! $user || ! Hash::check($request->password, $user->password)) {
                throw ValidationException::withMessages([
                    'login' => ['No se encontraron las credenciales ingresadas'],
                ]);
            }*/
            $token = $user->createToken($request->login)->plainTextToken;
            return response()->json([
                'user' => $user->load('person'),
                'token' => $token
            ], 200);
        /*if (!Auth::attempt($request->only('login', 'password'))) {
            $user = User::where('login', $request->login)->first();

            if (! $user || ! Hash::check($request->password, $user->password)) {
                throw ValidationException::withMessages([
                    'login' => ['No se encontraron las credenciales ingresadas'],
                ]);
            }
            $token = $user->createToken($request->login)->plainTextToken;
            return response()->json([
                'login' => true,
                'token' => $token
            ], 200);
        }else{
            $user = User::where('login', $request->login)->first();
            if (Hash::check($request->password, $user->password)) {
                return  $user->tokens->first()->plainTextToken;
                $token=DB::table('personal_access_tokens')
                ->where('tokenable_id', $user->id)
                ->first();
                return $token->currentAccessToken();
                return response()->json([
                    'login' => true,
                    'token' => hash('sha256',$token->token)
                ], 200);
            }
        }*/
        
    }

     /**
     * cerrar sesion
     *
     */    

    public function logout(Request $request){

       auth()->user()->tokens()->delete();
        
        return response()->json([
            'logout' => true,
            'message' => 'Sesion cerrada correctamente'
        ], 200);
    }
}
