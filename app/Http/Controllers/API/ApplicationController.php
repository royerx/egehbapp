<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Resource\Application;

class ApplicationController extends Controller
{

    /**
     * Listar pedidos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de pedidos
     */    
    public function index(Request $request)
    {
        $applications = Application::orderBy('id','DESC')
                            ->where('teacher_id',auth()->user()->load('person')->person->id)
                            ->get();
        return  response()->json($applications,200);
    }

    /**
     * Guardar un pedido
     *
     * 
     * @param  \App\Models\Resource\Application  $application
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y pedido guardado
     */
    public function store(Request $request,Application $application)
    {
        $teacher = auth()->user()->load('person')->person;

        $application = new Application();
        $application->type = $request->type;
        $application->date = $request->date;
        $application->grade = $request->grade;
        $application->section = $request->section;
        $application->place = $request->place;
        $application->teacher_id=$teacher->id;

        $application->save();
                
        return response()->json([
            'message' => 'Pedido registrado correctamente',
            'application' => $application
        ], 200);
    }

    /**
     * Mostrar un pedido
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Resource\Application  $application
     * @return \Illuminate\Http\Response: Json con el pedido encontrado
     */
    public function show(Request $request,Application $application)
    {
        return response()->json([
            'application' => $application
        ],200);
    }

    /**
     * Modificar un pedido
     *
     * @param  \App\Http\Requests\Resource\Application\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Resource\Application  $application
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el pedido modificado
     */
    public function update(Request $request, Application $application)
    {
        if($application->reception){
            return response()->json([
                'message' => 'No puede modificarse porque ya fue atendido',
                'application' => $application
            ], 200);
        }else{
            $teacher = auth()->user()->load('person')->person;

            $application->type = $request->type;
            $application->date = $request->date;
            $application->grade = $request->grade;
            $application->section = $request->section;
            $application->place = $request->place;
            $application->teacher_id=$teacher->id;

            $application->save();
                    
            return response()->json([
                'message' => 'Pedido modificado correctamente',
                'application' => $application
            ], 200);
        }
    }

    /**
     * Eliminar un pedido
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Resource\Application  $application
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Application $application)
    {
        if($application->delivery){
            return response()->json([
                'deleted' => 'no',
                'message' => 'No puede eliminarse porque ya esta atendido o esta siendo atendido',
                'application' => $application
            ], 200);
        }else{
            $application->delete();
             return response()->json([
                'deleted' => 'yes',
                'message' => 'Pedido eliminado correctamente',
                'application' => $application
            ], 200);
        }
    }

}
