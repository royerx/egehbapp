<?php

namespace App\Http\Controllers\API;

use App\Models\API\Incident;
use App\Models\Management\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class IncidentController extends Controller
{
    /**
     * registrar justificacion
     *
     * @return json con los datos del estudiante registrado
     */    

    public function store(Request $request,Incident $incident){
        
        $day = Carbon::create($request->date);
        $today = Carbon::now();
        $student = Student::where('dni',$request->dni)->first();

            $incident->date=$day;
            $incident->time=$today;
            $incident->detail=$request->detail;
            $incident->user_id=auth()->user()->load('person')->person->id;

            $student->incidents()->save($incident);
                
            return response()->json([
                'message' => 'Incidente registrado correctamente',
                'student' => $student
                ], 200);
    }   
}
