<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Management\Student;
use App\Models\API\Attendance;
use App\Models\System\Parameter;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AttendanceController extends Controller
{
    /**
     * marcar asistencia
     *
     * @return json con los datos del estudiante marcado
     */    

    public function record(Request $request){
        
        $today = Carbon::now();
        $student = Student::where('dni',$request->dni)->first();
        $parameterLate = Parameter::where('name',Parameter::LATE_TOLERANCE)->where('beginDate','<=',$today)->where('endDate','>=',$today)->first();
        $parameterAbsence = Parameter::where('name',Parameter::ABSENCE_TOLERANCE)->where('beginDate','<=',$today)->where('endDate','>=',$today)->first();
        $verify=Attendance::where('student_id',$student->id)->where('date',$today->toDateString())->first();

        if($today->weekDay()==5){
            return response()->json([
                'message' => 'Sábado no se marca asistencia',
                'student' => $student
            ], 200);
        }
        if($today->weekDay()==6){
             return response()->json([
                'message' => 'Domingo no se marca asistencia',
                'student' => $student
            ], 200);
        }

        if($verify){
            if($verify->type==Attendance::ABSENCE && !$verify->time){
                $verify->time=$today;
                $verify->type=Attendance::ATTENDANCE;
                $verify->user_id=auth()->user()->load('person')->person->id;
                $message='Asistencia registrada correctamente';
                if($today->toTimeString()>$parameterAbsence->value){
                    $verify->type=Attendance::ABSENCE;
                    $message='La asistencia se marcó como FALTÓ';
                }else if($today->toTimeString()>$parameterLate->value){
                    $verify->type=Attendance::LATE;
                    $message.= '  TARDE';
                }

                $verify->save();
                    
                return response()->json([
                    'message' => $message,
                    'student' => $student
                    ], 200);
            }else{
                return response()->json([
                    'message' => 'Ya marcó la asistencia',
                    'student' => $student
                ], 200);
            }
        }else{
            $attendance = new Attendance();
            $attendance->date=$today;
            $attendance->time=$today;
            $attendance->type=Attendance::ATTENDANCE;
            $attendance->user_id=auth()->user()->load('person')->person->id;
            $message='Asistencia registrada correctamente';
            if($today->toTimeString()>$parameterAbsence->value){
                $attendance->type=Attendance::ABSENCE;
                $message='La asistencia se marcó como FALTÓ';
            }else if($today->toTimeString()>$parameterLate->value){
                $attendance->type=Attendance::LATE;
                $message.= '  TARDE';
            }

            $student->attendances()->save($attendance);
                
            return response()->json([
                'message' => $message,
                'student' => $student
                ], 200);
        }
    }
    /**
     * obtener datos del estudiante por el DNI
     *
     * @return json con los datos del estudiante
     */    

    public function student(Request $request){
        $student = Student::where('dni',$request->dni)->first();
       
        return response()->json($student->load('periods'), 200);
    }
    /**
     * registrar justificacion
     *
     * @return json con los datos del estudiante registrado
     */    

    public function excuse(Request $request){
        
        $day = Carbon::create($request->date);
        $student = Student::where('dni',$request->dni)->first();
        $parameter = Parameter::where('name',Parameter::ENTRY_TIME)->where('beginDate','<=',$day)->where('endDate','>=',$day)->first();
        $verify=Attendance::where('student_id',$student->id)->where('date',$day->toDateString())->first();

        if($day->weekDay()==5){
            return response()->json([
                'message' => 'Sábado no se marca asistencia',
                'student' => $student
            ], 200);
        }
        if($day->weekDay()==6){
             return response()->json([
                'message' => 'Domingo no se marca asistencia',
                'student' => $student
            ], 200);
        }

        if($verify){
            if($verify->type==Attendance::LATE){
                $verify->type=Attendance::LATE_EXCUSE;
            }else if($verify->type==Attendance::ABSENCE){
                $verify->type=Attendance::ABSENCE_EXCUSE;
            }else{
                return response()->json([
                    'message' => 'Ya marcó la asistencia',
                    'student' => $student
                ], 200);
            }
            
            $verify->time=$parameter->value;
            $verify->user_id=auth()->user()->load('person')->person->id;
            $verify->detail=$request->excuse;
            $message='Justificación registrada correctamente';
            
            $verify->save();
                    
            return response()->json([
                'message' => $message,
                'student' => $student
            ], 200);
        }else{

            $attendance = new Attendance();
            $attendance->date=$day;
            $attendance->time=$parameter->value;
            $attendance->type=Attendance::ABSENCE_EXCUSE;
            $attendance->detail=$request->excuse;
            $attendance->user_id=auth()->user()->load('person')->person->id;

            $student->attendances()->save($attendance);
                
            return response()->json([
                'message' => 'Justificación registrada correctamente',
                'student' => $student
                ], 200);
        }
        
    }
    
    /**
     * obtener los estudiantes
     *
     * @return json con los estudiantes
     */    

    public function students(Request $request){
        $students = Student::with('periods')->getByRole()->get();
        return response()->json($students, 200);
    }
    
}
