<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Resource\Device;
use App\Http\Requests\Resource\Device\StoreRequest;
use App\Http\Requests\Resource\Device\UpdateRequest;

class DeviceController extends Controller
{
     public function __construct()
    {
        $this->authorizeResource(Device::class, 'device');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar recursos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de recursos
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $devices = Device::orderBy('id','DESC')
                                ->code($request->find)
                                ->name($request->find)
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($devices,200);
        }

        return redirect('home');
    }

    /**
     * Guardar un recurso
     *
     * @param  \App\Http\Requests\Resource\Device\StoreRequest  $request: request para guardado
     * @param  \App\Models\Resource\Device  $device
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y recurso guardado
     */
    public function store(StoreRequest $request,Device $device)
    {
        if($request->ajax()){
            $device=$device->store($request);
            if($device){
                return response()->json([
                    'message' => 'Recurso creado correctamente.',
                    'device' => $device
                ],200);
            }
            else{
                return abort(500);
            }
        }

        return redirect('home');
    }

    /**
     * Mostrar un recurso
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Resource\Device  $device
     * @return \Illuminate\Http\Response: Json con el recurso encontrado
     */
    public function show(Request $request,Device $device)
    {
        if($request->ajax()){
            return response()->json([
                'device' => $device
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un recurso
     *
     * @param  \App\Http\Requests\Resource\Device\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Resource\Device  $device
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el recurso modificado
     */
    public function update(UpdateRequest $request, Device $device)
    {
        if($request->ajax()){
            $device=$device->update($request->all());
            return response()->json([
                'message' => 'Recurso actualizado correctamente.',
                'device' => $device
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un recurso
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Resource\Device  $device
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Device $device)
    {
        if($request->ajax()){
            $device = $device->delete();
            return response()->json([
                'message' => 'Recurso eliminado correctamente.',
                'device' => $device
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Guardar foto del recurso
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Resource\Device  $device
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con el recurso y su foto actualizado
     */
    public function updatePhoto(Request $request,Device $device)
    {
        $this->authorize('updatePhoto', $device);
        if($request->ajax()){
            if($request->photo){
                $device->picture = $device->savePicture($request->photo,Device::PICTURE_PATH,$device->picture);
                $device->save();
                return response()->json([
                    'message' => 'Foto actualizada correctamente.',
                    'device' => $device,
                    'picture' => $device->picture
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                    'device' => $device
                ],500);
            }
        }

        return redirect('home');
        
    }
    /**
     * Listar todos los recursos dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de recursos
     */
    public function list(Request $request)
    {
        $this->authorize('viewAny', Device::class);
        if($request->ajax()){

            if($request->quantity)
                $devices = Device::orderBy('id','DESC')
                                    ->code($request->find)
                                    ->name($request->find)
                                    ->take($request->quantity)->get();
            else
                $devices = Device::orderBy('id','DESC')
                                    ->code($request->find)
                                    ->name($request->find)
                                    ->get();

            return  response()->json($devices,200);
        }
        
        return redirect('home');
    }
    /**
     * Listar todos los nombres
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de nombres
     */
    public function names(Request $request)
    {
        $this->authorize('create', Device::class)||$this->authorize('update', Device::class);
        if($request->ajax()){
            $find = Device::select($request->name)->whereNotNull($request->name)->distinct()->pluck($request->name);

            return  response()->json($find,200);
        }
        
        return redirect('home');
    }

    /**
     * Listar todos los recursos dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de recursos
     */
    public function getByCode(Request $request)
    {
        $this->authorize('getByCode', new Device());
        if($request->ajax()){

            $device = Device::where('code',$request->code)->first();
       
            return response()->json($device, 200);
        }
        
        return redirect('home');
    }
}
