<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Management\Student;
use App\Models\Management\Period;
use App\Models\API\Incident;
use App\Models\Resource\Application;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use DateTime;
use DateInterval;
use DatePeriod;
use App\Models\System\Parameter;
use App\Models\API\Attendance;

class ReportController extends Controller
{
    
    public function attendance(Request $request){

        if($request->ajax()){
            $comienzo = new DateTime($request->beginDate);
            $final = new DateTime($request->endDate);
            // Necesitamos modificar la fecha final en 1 día para que aparezca en el bucle
            $final = $final->modify('+1 day');

            $intervalo = DateInterval::createFromDateString('1 day');
            $periodo = new DatePeriod($comienzo, $intervalo, $final);

            foreach ($periodo as $dt) {
                $day = Carbon::create($dt);
                    $students = Student::WhereHas('lessons',function($query) use($request){
                                    $query->Where('grade',$request->grade)->where('section',$request->section)->where('period_id',Period::getCurrent()->id);
                                })->whereDoesntHave('attendances', function($query) use($day) {
                                    $query->where('date', $day);
                                })->orderBy('lastName')->orderBy('firstName')->get();
                    foreach($students as $student){
                        $attendance = new Attendance();
                        $attendance->date=$day;
                        $attendance->type=Attendance::ABSENCE;
                        $attendance->detail='';
                        $attendance->user_id=auth()->user()->load('person')->person->id;

                        if($day->weekDay() ==5 || $day->weekDay() ==6){
                            $attendance->type='';
                        }
                        $student->attendances()->save($attendance);
                    }
            }
            $students = Student::WhereHas('lessons',function($query) use($request){
                                $query->Where('grade',$request->grade)->where('section',$request->section)->where('period_id',Period::getCurrent()->id);
                            })->with(['attendances' => function($query) use($request) {
                                $query->whereBetween('date', [$request->beginDate, $request->endDate])->orderBy('date');
                            }])->orderBy('lastName')->orderBy('firstName')->get();
            return response()->json($students, 200);
        }
        return redirect('home');
    }
    
    public function student(Request $request, Student $student){

        if($request->ajax()){
            $comienzo = new DateTime($request->beginDate);
            $final = new DateTime($request->endDate);
            // Necesitamos modificar la fecha final en 1 día para que aparezca en el bucle
            $final = $final->modify('+1 day');

            $intervalo = DateInterval::createFromDateString('1 day');
            $periodo = new DatePeriod($comienzo, $intervalo, $final);
            $studentPeriod=Student::where('id',$student->id)->WhereHas('lessons',function($query) use($request){
                                    $query->where('period_id',Period::getCurrent()->id);
                                })->count();
            if($studentPeriod){
                foreach ($periodo as $dt) {
                    $day = Carbon::create($dt);
                        $attendanceVerify = Attendance::where('date',$day)->where('student_id',$student->id)->count();
                        if(!$attendanceVerify){
                            $attendance = new Attendance();
                            $attendance->date=$day;
                            $attendance->type=Attendance::ABSENCE;
                            $attendance->detail='';
                            $attendance->user_id=auth()->user()->load('person')->person->id;

                            if($day->weekDay() ==5 || $day->weekDay() ==6){
                                $attendance->type='';
                            }
                            $student->attendances()->save($attendance);
                        }
                }
            }
             $student->load(['attendances' => function($query) use($request) {
                                $query->whereBetween('date', [$request->beginDate, $request->endDate]);
                            },'incidents'=>function($query) use($request){
                                $query->whereBetween('date',[$request->beginDate,$request->endDate]);
                            },'lessons'=>function($query){
                                $query->Where('period_id',Period::getCurrent()->id);
                            },'incidents.person']);
            return response()->json($student, 200); 
        }
        return redirect('home');
    }
    
    public function notebook(Request $request){
        if($request->ajax()){
            $incidents = Incident::whereBetween('date',[$request->beginDate,$request->endDate])->with(['person','student.lessons'=>function($query){
                                $query->Where('period_id',Period::getCurrent()->id);
                            }])->get();
            return response()->json($incidents, 200);
        }
        return redirect('home');
    }

    public function application(Request $request){
        if($request->ajax()){
            $applications = Application::where('date',$request->date)->with('teacher','device')->get();
            return response()->json($applications, 200);
        }
        return redirect('home');
    }

    public function absences(Request $request){
        if($request->ajax()){
            $comienzo = new DateTime($request->beginDate);
            $final = new DateTime($request->endDate);
            // Necesitamos modificar la fecha final en 1 día para que aparezca en el bucle
            $final = $final->modify('+1 day');

            $intervalo = DateInterval::createFromDateString('1 day');
            $periodo = new DatePeriod($comienzo, $intervalo, $final);

            foreach ($periodo as $dt) {
                $day = Carbon::create($dt);
                    $students = Student::WhereHas('lessons',function($query) use($request){
                                    $query->where('period_id',Period::getCurrent()->id);
                                })->whereDoesntHave('attendances', function($query) use($day) {
                                    $query->where('date', $day);
                                })->orderBy('lastName')->orderBy('firstName')->get();
                    foreach($students as $student){
                        $attendance = new Attendance();
                        $attendance->date=$day;
                        $attendance->type=Attendance::ABSENCE;
                        $attendance->detail='';
                        $attendance->user_id=auth()->user()->load('person')->person->id;

                        if($day->weekDay() ==5 || $day->weekDay() ==6){
                            $attendance->type='';
                        }
                        $student->attendances()->save($attendance);
                    }
            }
            $students = Student::WhereHas('lessons',function($query) use($request){
                                $query->where('period_id',Period::getCurrent()->id);
                            })->WhereHas('attendances', function($query) use($request) {
                                $query->whereBetween('date', [$request->beginDate, $request->endDate])->where('type','Falta');
                            })->with(['lessons' => function($query) use($request){
                                $query->where('period_id',Period::getCurrent()->id);
                            } ,'attendances'=> function($query) use($request){
                                $query->whereBetween('date', [$request->beginDate, $request->endDate])->where('type','Falta')->orderBy('date');
                            }])->orderBy('lastName')->orderBy('firstName')->get();

            return response()->json($students, 200);
        }
        return redirect('home');
    }
}
