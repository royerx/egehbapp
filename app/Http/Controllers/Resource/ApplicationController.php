<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Resource\Application;

class ApplicationController extends Controller
{
     public function __construct()
    {
        $this->authorizeResource(Application::class, 'application');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar pedidos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de pedidos
     */    
    public function index(Request $request)
    {
        
        if($request->ajax()){
            if($request->viewAll=='true'){
                $applications = Application::with('teacher','device')
                                ->teacher($request->find)
                                ->orderBy('id','DESC')
                                ->paginate(env("PAGE_NUMBER",20));
            }else{
                $applications = Application::with('teacher','device')
                                ->teacher($request->find)
                                ->where('date',$request->date)
                                ->orderBy('id','DESC')
                                ->paginate(env("PAGE_NUMBER",20));
            }
        
            return  response()->json($applications,200);
        }
        return redirect('home');
    }

    /**
     * Guardar un pedido
     *
     * 
     * @param  \App\Models\Resource\Application  $application
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y pedido guardado
     */
    public function store(Request $request,Application $application)
    {
        if($request->ajax()){
            $application = new Application();
            $application->type = $request->type;
            $application->date = $request->date;
            $application->grade = $request->grade;
            $application->section = $request->section;
            $application->place = $request->place;
            $application->teacher_id=$request->teacher['id'];

            $application->save();
                    
            return response()->json([
                'message' => 'Pedido registrado correctamente',
                'application' => $application->load('teacher','device')
            ], 200);   
        }
        return redirect('home');
    }

    /**
     * Mostrar un pedido
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Resource\Application  $application
     * @return \Illuminate\Http\Response: Json con el pedido encontrado
     */
    public function show(Request $request,Application $application)
    {
        if($request->ajax()){
            return response()->json([
                'application' => $application->load('teacher','device')
            ],200);
        }
        return redirect('home');
    }

    /**
     * Modificar un pedido
     *
     * @param  \App\Http\Requests\Resource\Application\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Resource\Application  $application
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el pedido modificado
     */
    public function update(Request $request, Application $application)
    {
        if($request->ajax()){
            if($application->reception){
                return response()->json([
                    'message' => 'No puede modificarse porque ya fue atendido',
                    'application' => $application
                ], 200);
            }else{
                $request['teacher_id']=$request->teacher['id'];
                $request['device_id']=$request->device['id'];
                if($application->state=='Registrado'){
                    $request['delivery']= Carbon::now();
                }
                if($application->state=='Recibido'){
                    $request['reception']= Carbon::now();
                }
                $application->update($request->all());
                return response()->json([
                    'message' => 'Pedido modificado correctamente',
                    'application' => $application
                ], 200);
            }
        }
        return redirect('home');
    }

    /**
     * Eliminar un pedido
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Resource\Application  $application
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Application $application)
    {
        if($request->ajax()){
            if($application->delivery){
                return response()->json([
                    'deleted' => 'no',
                    'message' => 'No puede eliminarse porque ya esta atendido o esta siendo atendido',
                    'application' => $application
                ], 200);
            }else{
                $application->delete();
                return response()->json([
                    'deleted' => 'yes',
                    'message' => 'Pedido eliminado correctamente',
                    'application' => $application
                ], 200);
            }
        }
        return redirect('home');
    }

}
