<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\System\Parameter;
use App\Http\Requests\System\Parameter\StoreRequest;

class ParameterController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Parameter::class, 'parameter');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar parametros
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de parametros
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $parameters=Parameter::orderBy('id','DESC')
                                ->name($request->find)
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($parameters,200);
        }

        return redirect('home');
    }

    /**
     * Guardar un parametro
     *
     * @param  \App\Http\Requests\System\Parameter\StoreRequest  $request: request para guardado
     * @param  \App\Models\System\Parameter  $parameter
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion, parametro guardado
     */
    public function store(StoreRequest $request,Parameter $parameter)
    {
        if($request->ajax()){
            $parameter=$parameter->create($request->all());
            return response()->json([
                'message' => 'Parametro creado correctamente.',
                'parameter' => $parameter
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un parametro
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\System\Parameter  $parameter
     * @return \Illuminate\Http\Response: Json con la parametro encontrado
     */
    public function show(Request $request,Parameter $parameter)
    {
        if($request->ajax()){
            return response()->json([
                'parameter' => $parameter
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un parametro
     *
     * @param  \App\Http\Requests\System\Parameter\StoreRequest  $request: request para modificado
     * @param  \App\Models\System\Parameter  $parameter
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion, parametro y candidatos modificado
     */
    public function update(StoreRequest $request, Parameter $parameter)
    {
        if($request->ajax()){
            $parameter=$parameter->update($request->all());
            return response()->json([
                'message' => 'Parametro actualizado correctamente.',
                'parameter' => $parameter
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un parametro
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\System\Parameter  $parameter
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Parameter $parameter)
    {
        if($request->ajax()){
            $parameter = $parameter->delete();
            return response()->json([
                'message' => 'Parametro eliminado correctamente.',
                'parameter' => $parameter
            ],200);
        }

        return redirect('home');
    }
}
