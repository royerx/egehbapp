<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Management\Student;
use App\Http\Requests\Management\Student\StoreRequestStudent;
use App\Http\Requests\Management\Student\UpdateRequest;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Student::class, 'student');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar estudiantes
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de estudiantes
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $students = Student::with('periods')
                                    ->orderBy('id','DESC')
                                    ->firsName($request->find)
                                    ->lastName($request->find)
                                    ->dni($request->find)
                                    ->email($request->find)
                                    ->getByRole()
                                    ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($students,200);
        }

        return redirect('home');
    }

    /**
     * Guardar un estudiante
     *
     * @param  \App\Http\Requests\Management\Student\StoreRequestStudent  $request: request para guardado
     * @param  \App\Models\Management\Student  $student
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y estudiante guardado
     */
    public function store(StoreRequestStudent $request,Student $student)
    {
        if($request->ajax()){
            $student=$student->store($request);
            if($student){
                return response()->json([
                    'message' => 'Estudiante creado correctamente.',
                    'student' => $student
                ],200);
            }
            else{
                return abort(500);
            }
        }

        return redirect('home');
    }

    /**
     * Mostrar un estudiante
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Student  $student
     * @return \Illuminate\Http\Response: Json con el estudiante encontrado
     */
    public function show(Request $request,Student $student)
    {
        if($request->ajax()){
            return response()->json([
                'student' => $student->load('periods')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un estudiante
     *
     * @param  \App\Http\Requests\Management\Student\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Management\Student  $student
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el estudiante modificado
     */
    public function update(UpdateRequest $request, Student $student)
    {
        if($request->ajax()){
            $student=$student->update($request->all());
            return response()->json([
                'message' => 'Estudiante actualizado correctamente.',
                'student' => $student
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un estudiante
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Student  $student
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Student $student)
    {
        if($request->ajax()){
            $student = $student->remove($student);
            return response()->json([
                'message' => 'Estudiante eliminado correctamente.',
                'student' => $student
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Guardar foto del estudiante
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Student  $student
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con el estudiante y su foto actualizada 
     */
    public function updatePhoto(Request $request,Student $student)
    {
        $this->authorize('updatePhoto', $student);
        if($request->ajax()){
            if($request->photo){
                $student->picture = $student->savePicture($request->photo,Student::PICTURE_PATH,$student->picture);
                $student->save();
                return response()->json([
                    'message' => 'Foto actualizada correctamente.',
                    'student' => $student,
                    'picture' => $student->picture
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                    'student' => $student
                ],500);
            }
        }

        return redirect('home');
        
    }
    /**
     * Listar todos los estudiantes dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de estudiantes
     */
    public function list(Request $request)
    {
        $this->authorize('viewAny', Student::class);
        if($request->ajax()){

            if($request->quantity)
                $students = Student::orderBy('id','DESC')
                                    ->firsName($request->find)
                                    ->lastName($request->find)
                                    ->dni($request->find)
                                    ->email($request->find)
                                    ->getByRole()
                                    ->take($request->quantity)->get();
            else
                $students = Student::orderBy('id','DESC')
                                    ->firsName($request->find)
                                    ->lastName($request->find)
                                    ->dni($request->find)
                                    ->getByRole()
                                    ->email($request->find)->get();

            return  response()->json($students,200);
        }
        
        return redirect('home');
    }
}
