<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Management\Period\StoreRequest;
use App\Http\Requests\Management\Period\UpdateRequest;
use App\Models\Management\Period;

class PeriodController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Period::class, 'period');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar periodos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de periodos
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $periods=Period::orderBy('id','DESC')
                                ->name($request->find)
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($periods,200);
        }

        return redirect('home');
    }

    /**
     * Guardar una periodo
     *
     * @param  \App\Http\Requests\Election\Period\StoreRequest  $request: request para guardado
     * @param  \App\Models\Election\Period  $period
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion, periodo guardado
     */
    public function store(StoreRequest $request,Period $period)
    {
        if($request->ajax()){
            $period=$period->store($request);
            return response()->json([
                'message' => 'Periodo Académico creado correctamente.',
                'period' => $period
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un periodo
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\Period  $period
     * @return \Illuminate\Http\Response: Json con la periodo encontrado
     */
    public function show(Request $request,Period $period)
    {
        if($request->ajax()){
            return response()->json([
                'period' => $period
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una periodo
     *
     * @param  \App\Http\Requests\Election\Period\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Election\Period  $period
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion, periodo y candidatos modificado
     */
    public function update(UpdateRequest $request, Period $period)
    {
        if($request->ajax()){
            $period=$period->update($request->all());
            return response()->json([
                'message' => 'Periodo Académico actualizado correctamente.',
                'period' => $period
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar una periodo
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\Period  $period
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Period $period)
    {
        if($request->ajax()){
            $period = $period->delete();
            return response()->json([
                'message' => 'Periodo Académico eliminado correctamente.',
                'period' => $period
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Listar todos los periodos dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de periodos
     */
    public function list(Request $request)
    {
        $this->authorize('viewAny', Period::class);
        if($request->ajax()){

            if($request->quantity)
                $periods = Period::orderBy('id','DESC')->name($request->find)->take($request->quantity)->get();
            else
                $periods = Period::orderBy('id','DESC')->name($request->find)->get();

            return  response()->json($periods,200);
        }
        
        return redirect('home');
    }
    /**
     * Listar todos los periodos dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de periodos
     */
    public function setActive(Request $request,Period $period)
    {
        $this->authorize('update', $period);
        if($request->ajax()){
            $period->setInactive();
            $period->active=true;
            $period->save();
            return  response()->json(['msg'=>'Periodo activado correctamente'],200);
        }
        
        return redirect('home');
    }
}
