<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Management\Person;

class PersonController extends Controller
{
    /**
     * Listar todas las personas dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de personas
     */
    public function index(Request $request)
    {
        if($request->ajax()){

            if($request->quantity)
                $people = Person::orderBy('id','DESC')
                                    ->findForUser($request->find)
                                    ->doesntHave('user')
                                    ->take($request->quantity)
                                    ->get();
            else
                $people = Person::orderBy('id','DESC')
                                    ->findForUser($request->find)
                                    ->doesnthave('user')
                                    ->get();

            return  response()->json($people,200);
        }
        
        return redirect('home');
    }
}
