<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Management\Administrative;
use Illuminate\Http\Request;
use App\Http\Requests\Management\Administrative\StoreRequestAdministrative;
use App\Http\Requests\Management\Administrative\UpdateRequest;

class AdministrativeController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Administrative::class, 'administrative');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar administrativos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de administrativos
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $administratives = Administrative::orderBy('id','DESC')
                                    ->firsName($request->find)
                                    ->lastName($request->find)
                                    ->dni($request->find)
                                    ->email($request->find)
                                    ->getByRole()
                                    ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($administratives,200);
        }

        return redirect('home');
    }

    /**
     * Guardar un administrativo
     *
     * @param  \App\Http\Requests\Management\Administrative\StoreRequestAdministrative  $request: request para guardado
     * @param  \App\Models\Management\Administrative  $administrative
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y administrativo guardado
     */
    public function store(StoreRequestAdministrative $request,Administrative $administrative)
    {
        if($request->ajax()){
            $administrative=$administrative->store($request);
            if($administrative){
                return response()->json([
                    'message' => 'Administrativo creado correctamente.',
                    'administrative' => $administrative
                ],200);
            }
            else{
                return abort(500);
            }
        }

        return redirect('home');
    }

    /**
     * Mostrar un administrativo
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Administrative  $administrative
     * @return \Illuminate\Http\Response: Json con el administrativo encontrado
     */
    public function show(Request $request,Administrative $administrative)
    {
        if($request->ajax()){
            return response()->json([
                'administrative' => $administrative
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un administrativo
     *
     * @param  \App\Http\Requests\Management\Administrative\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Management\Administrative  $administrative
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el administrativo modificado
     */
    public function update(UpdateRequest $request, Administrative $administrative)
    {
        if($request->ajax()){
            $administrative=$administrative->update($request->all());
            return response()->json([
                'message' => 'Administrativo actualizado correctamente.',
                'administrative' => $administrative
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un administrativo
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Administrative  $administrative
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Administrative $administrative)
    {
        if($request->ajax()){
            $administrative = $administrative->remove($administrative);
            return response()->json([
                'message' => 'Administrativo eliminado correctamente.',
                'administrative' => $administrative
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Guardar foto del administrativo
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Administrative  $administrative
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con el administrativo y su foto actualizada 
     */
    public function updatePhoto(Request $request,Administrative $administrative)
    {
        $this->authorize('updatePhoto', $administrative);
        if($request->ajax()){
            if($request->photo){
                $administrative->picture = $administrative->savePicture($request->photo,Administrative::PICTURE_PATH,$administrative->picture);
                $administrative->save();
                return response()->json([
                    'message' => 'Foto actualizada correctamente.',
                    'administrative' => $administrative,
                    'picture' => $administrative->picture
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                    'administrative' => $administrative
                ],500);
            }
        }

        return redirect('home');
        
    }
}
