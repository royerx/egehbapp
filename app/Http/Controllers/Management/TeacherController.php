<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Management\Teacher;
use Illuminate\Http\Request;
use App\Http\Requests\Management\Teacher\StoreRequestTeacher;
use App\Http\Requests\Management\Teacher\UpdateRequest;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Teacher::class, 'teacher');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar profesores
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de profesores
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $teachers = Teacher::orderBy('id','DESC')
                                    ->firsName($request->find)
                                    ->lastName($request->find)
                                    ->dni($request->find)
                                    ->email($request->find)
                                    ->getByRole()
                                    ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($teachers,200);
        }

        return redirect('home');
    }

    /**
     * Guardar un profesor
     *
     * @param  \App\Http\Requests\Management\Teacher\StoreRequestTeacher  $request: request para guardado
     * @param  \App\Models\Management\Teacher  $teacher
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y profesor guardado
     */
    public function store(StoreRequestTeacher $request,Teacher $teacher)
    {
        if($request->ajax()){
            $teacher=$teacher->store($request);
            if($teacher){
                return response()->json([
                    'message' => 'Profesor creado correctamente.',
                    'teacher' => $teacher
                ],200);
            }
            else{
                return abort(500);
            }
        }

        return redirect('home');
    }

    /**
     * Mostrar un profesor
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Teacher  $teacher
     * @return \Illuminate\Http\Response: Json con el profesor encontrado
     */
    public function show(Request $request,Teacher $teacher)
    {
        if($request->ajax()){
            return response()->json([
                'teacher' => $teacher
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un profesor
     *
     * @param  \App\Http\Requests\Management\Teacher\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Management\Teacher  $teacher
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el profesor modificado
     */
    public function update(UpdateRequest $request, Teacher $teacher)
    {
        if($request->ajax()){
            $teacher=$teacher->update($request->all());
            return response()->json([
                'message' => 'Profesor actualizado correctamente.',
                'teacher' => $teacher
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un profesor
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Teacher  $teacher
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Teacher $teacher)
    {
        if($request->ajax()){
            $teacher = $teacher->remove($teacher);
            return response()->json([
                'message' => 'Profesor eliminado correctamente.',
                'teacher' => $teacher
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Guardar foto del profesor
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Teacher  $teacher
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con el profesor y su foto actualizada 
     */
    public function updatePhoto(Request $request,Teacher $teacher)
    {
        $this->authorize('updatePhoto', $teacher);
        if($request->ajax()){
            if($request->photo){
                $teacher->picture = $teacher->savePicture($request->photo,Teacher::PICTURE_PATH,$teacher->picture);
                $teacher->save();
                return response()->json([
                    'message' => 'Foto actualizada correctamente.',
                    'teacher' => $teacher,
                    'picture' => $teacher->picture
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                    'teacher' => $teacher
                ],500);
            }
        }

        return redirect('home');
        
    }
    /**
     * Listar todos los docentes dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de docentes
     */
    public function list(Request $request)
    {
        $this->authorize('viewAny', Teacher::class);
        if($request->ajax()){

            if($request->quantity)
                $teachers = Teacher::orderBy('id','DESC')
                                    ->firsName($request->find)
                                    ->lastName($request->find)
                                    ->dni($request->find)
                                    ->email($request->find)
                                    ->getByRole()
                                    ->take($request->quantity)->get();
            else
                $teachers = Teacher::orderBy('id','DESC')
                                    ->firsName($request->find)
                                    ->lastName($request->find)
                                    ->dni($request->find)
                                    ->getByRole()
                                    ->email($request->find)->get();

            return  response()->json($teachers,200);
        }
        
        return redirect('home');
    }
}
