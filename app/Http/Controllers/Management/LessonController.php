<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Models\Management\Lesson;
use Illuminate\Http\Request;
use App\Http\Requests\Management\Lesson\StoreRequest;

class LessonController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Lesson::class, 'lesson');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar gado y seccion
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de gado y seccion
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $lessons=Lesson::orderBy('id','DESC')
                                ->get();
            return  response()->json($lessons,200);
        }

        return redirect('home');
    }

    /**
     * Guardar un gado y seccion
     *
     * @param  \App\Http\Requests\Management\Lesson\StoreRequest  $request: request para guardado
     * @param  \App\Models\Management\Lesson  $lesson
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion, gado y seccion guardado
     */
    public function store(StoreRequest $request,Lesson $lesson)
    { 
        if($request->ajax()){
            $lesson=$lesson->create(array_merge($request->all(),['student_id'=>$request->student['id'],'period_id'=>$request->period['id']]));
            return response()->json([
                'message' => 'Grado y Sección creado correctamente.',
                'lesson' => $lesson
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un gado y seccion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Lesson  $lesson
     * @return \Illuminate\Http\Response: Json con la gado y seccion encontrado
     */
    public function show(Request $request,Lesson $lesson)
    {
        if($request->ajax()){
            return response()->json([
                'lesson' => $lesson->load('period','student')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una gado y seccion
     *
     * @param  \App\Http\Requests\Management\Lesson\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Management\Lesson  $lesson
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion, gado y seccion y candidatos modificado
     */
    public function update(StoreRequest $request, Lesson $lesson)
    {
        if($request->ajax()){
            $lesson=$lesson->update(array_merge($request->all(),['student_id'=>$request->student['id'],'period_id'=>$request->period['id']]));
            return response()->json([
                'message' => 'Grado y Sección actualizado correctamente.',
                'lesson' => $lesson
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar una gado y seccion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Lesson  $lesson
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Lesson $lesson)
    {
        if($request->ajax()){
            $lesson = $lesson->delete();
            return response()->json([
                'message' => 'Grado y Sección eliminado correctamente.',
                'lesson' => $lesson
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Listar todos los gado y seccion dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de gado y seccion
     */
    public function list(Request $request)
    {
        $this->authorize('viewAny', Lesson::class);
        if($request->ajax()){

            if($request->quantity)
                $lessons = Lesson::orderBy('id','DESC')->name($request->find)->take($request->quantity)->get();
            else
                $lessons = Lesson::orderBy('id','DESC')->name($request->find)->get();

            return  response()->json($lessons,200);
        }
        
        return redirect('home');
    }
}
