<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Security\User;
use App\Models\Security\Role;
use App\Models\Security\Permission;
use App\Models\Management\Period;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AdminController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }

     /**
     * Vista de página principal
     *
     * @return \Illuminate\Http\Response: Vista de la página principal
     */   
    public function index(){
        return view('pages.home');
    }

    /**
     * Obtener usuario logueado
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con el usuario y el rol asignados
     */    
    public function currentUser(Request $request)
    {
        if($request->ajax()){
            $user = User::findOrFail(Auth::user()->id);
            if(session('role_id')){
                $role = Role::findOrFail(session('role_id'))->load('permissionActions');
            }else{
                $role = "";
            }
            
            return response()->json([
                'user' => $user->load('person'),
                'role' => $role
            ],200);
        }

        return redirect('home');
    }
    
    /**
     * Roles del usuario logueado
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de roles del usuario logueado
     */    
    public function userRoles(Request $request)
    {
        if($request->ajax()){
            $user = Auth::user()->load('roles');
            return response()->json([
                'roles' => $user->roles
            ],200);
        }

        return redirect('home');
    }

    /**
     * Asignar rol al usuario logueado
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */    
    public function assignRole(Request $request)
    {
        if($request->ajax()){
            Session::put([
                'role_id' => $request->id,
            ]);
            return response()->json([
                'message' => 'ok'
            ],200);
        }

        return redirect('home');
    }

    /**
     * Verificar existencia de rol seleccionado
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Vista de la página de cedula de votación
     */    
    public function selectedRole(Request $request){
        if($request->ajax()){
            if(session('role_id')){
                $msg = "yes";
            }else{
                $msg = "no";
            }
            return response()->json([
                "message" => $msg
            ],200);
        }

        return redirect('home');
    }

    /**
     * Obtener periodo academico del año
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */    
    public function getCurrentPeriod(Request $request){
        if($request->ajax()){
            $period=Period::getCurrent();
            return response()->json([
                "period" => $period
            ],200);
        }

        return redirect('home');
    }
}
