<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use App\Http\Requests\Security\Role\StoreRequest;
use App\Http\Requests\Security\Role\UpdateRequest;
use Illuminate\Support\Str;
use App\Models\Security\Role;
use App\Models\Security\Permission;
use Illuminate\Http\Request;


class RoleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Role::class, 'role');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

     /**
     * Listar roles
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de roles
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $roles = Role::with('permissionActions')->orderBy('id','DESC')->name($request->find)->paginate(env("PAGE_NUMBER",20));
            return  response()->json($roles,200);
        }

        return redirect('home');
    }

   /**
     * Guardar un rol
     *
     * @param  \App\Http\Requests\Security\Role\StoreRequest  $request: request para guardado
     * @param  \App\Models\Security\Role  $role
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y rol guardado
     */
    public function store(StoreRequest $request,Role $role)
    {
        if($request->ajax()){
            $role=$role->store($request);
            return response()->json([
                'message' => 'Rol creado correctamente.',
                'role' => $role->load('permissionActions')
            ],200);
        }

        return redirect('home');
    }

     /**
     * Mostrar un rol
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\Role  $role
     * @return \Illuminate\Http\Response: Json con el rol encontrado
     */
    public function show(Request $request,Role $role)
    {
        if($request->ajax()){
            return response()->json([
                'role' => $role->load('permissionActions')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un rol
     *
     * @param  \App\Http\Requests\Security\Role\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Security\Role  $role
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el rol modificado
     */
    public function update(UpdateRequest $request, Role $role)
    {
        if($request->ajax()){
            $role=$role->my_update($request);
            return response()->json([
                'message' => 'Rol actualizado correctamente.',
                'role' => $role
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un rol
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\Role  $role
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Role $role)
    {
        if($request->ajax()){
            $role=$role->remove($role);
            return response()->json([
                'message' => 'Rol eliminado correctamente.',
                'role' => $role
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Asignar permisos a un rol
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\Role  $role
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con los permisos asignados
     */
    public function savePermissions(Request $request,Role $role)
    {
        $this->authorize('savePermissions', $role);
        if($request->ajax()){
            $permissionActions=collect($request->input('permission_actions'))->pluck('id');
            $role->permissionActions()->sync($permissionActions);
            return response()->json([
                'message' => 'Permisos guardados correctamente.',
                'role' => $role->load('permissionActions')
            ],200);
            
        }
        
        return redirect('home');
    }

    /**
     * Listar todos los roles dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de roles
     */
    public function list(Request $request)
    {
        $this->authorize('list', Role::class);
        if($request->ajax()){

            if($request->quantity)
                $roles = Role::with('permissionActions')->orderBy('id','DESC')->name($request->find)->take($request->quantity)->get();
            else
                $roles = Role::with('permissionActions')->orderBy('id','DESC')->name($request->find)->get();

            return  response()->json($roles,200);
        }
        
        return redirect('home');
    }
}
