<?php

namespace App\Http\Controllers\Election;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Election\Candidate;
use App\Http\Requests\Election\Candidate\StoreRequest;

class CandidateController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Candidate::class, 'candidate');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Guardar un candidato
     *
     * @param  \App\Http\Requests\Election\Position\StoreRequest  $request: request para guardado
     * @param  \App\Models\Election\Candidate  $candidate
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y candidato guardado
     */
    public function store(StoreRequest $request,Candidate $candidate)
    {
        if($request->ajax()){
            $candidate= $candidate->store($request);
            return response()->json([
                'message' => 'Candidato creado correctamente.',
                'candidate' => $candidate->load('student','position')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un candidato
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\Candidate  $candidate
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Candidate $candidate)
    {
        if($request->ajax()){
            $candidate=$candidate->delete();
            return response()->json([
                'message' => 'Candidato eliminado correctamente.',
                'candidate' => $candidate
            ],200);
        }

        return redirect('home');
    }
}
