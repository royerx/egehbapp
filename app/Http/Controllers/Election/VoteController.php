<?php

namespace App\Http\Controllers\Election;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Election\Vote;
use App\Models\Management\Period;
use PDF;

class VoteController extends Controller
{
    /**
     * Listar asistencia a votacion
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de votacion
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $period_id=$request->id;
            $result = Vote::result($period_id);
            return  response()->json($result,200);
        }

        return redirect('home');
    }

    /**
     * Guardar una votacion
     *
     * @param  \App\Http\Requests\Election\Vote\StoreRequest  $request: request para guardado
     * @param  \App\Models\Election\Vote  $vote
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion para mostrar la votacion guardada
     */
    public function store(Request $request,Vote $vote)
    {
        if($request->ajax()){
            $vote=$vote->store($request);
                return response()->json([
                    'message' => 'Votación realizada correctamente',
                    'vote' => $vote,
                ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar una votacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\VoteAttendance  $voteAttendance
     * @return \Illuminate\Http\Response: Json con la votacion encontrada
     */
    public function show(Request $request,Vote $vote)
    {
        if($request->ajax()){
            return response()->json([
                'vote' => $vote->load('period','group')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una votacion
     *
     * @param  \App\Http\Requests\Election\Vote\StoreRequest  $request: request para modificado
     * @param  \App\Models\Election\Vote  $vote
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y votacion modificada
     */
    public function update(StoreRequest $request, Vote $vote)
    {
        if($request->ajax()){
            $vote=$vote->my_update($request,$vote);
            return response()->json([
                'message' => 'Votación actualizada correctamente.',
                'vote' => $vote
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar una votacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\Vote  $vote
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Vote $vote)
    {
        if($request->ajax()){
            $vote = $vote->remove();
            return response()->json([
                'message' => 'Votacion eliminada correctamente.',
                'vote' => $vote
            ],200);
        }

        return redirect('home');
    }
    /*
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Management\Period  $period
     * @return pdf vista de los resultados electorales
     */
    public function result(Request $request,Period $period)
    {
        $result = Vote::result($period->id);
        $pdf = PDF::loadView('pages.report.result',compact('result','period'))->setPaper('a4','landscape');
        return $pdf->stream();
    }
}
