<?php

namespace App\Http\Controllers\Election;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Election\VoteAttendance\StoreRequest;
use App\Models\Election\VoteAttendance;
use App\Models\Management\Student;

class VoteAttendanceController extends Controller
{
    /**
     * Listar asistencia a votacion
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de asistencia
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $period=$request->period;
            $voteAttendance=VoteAttendance::with('student')
                                ->whereHas('period',function($query) use($period){
                                    $query->where('name',$period);
                                })
                                ->orderBy('id','DESC')
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($groups,200);
        }

        return redirect('home');
    }

    /**
     * Guardar una asistencia a votacion
     *
     * @param  \App\Http\Requests\Election\VoteAttendance\StoreRequest  $request: request para guardado
     * @param  \App\Models\Election\VoteAttendance  $voteAttendance
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion para mostrar la cedula de votacion, el mensaje a ser mostrado
     *                                      y los datos del estudiante
     */
    public function store(StoreRequest $request,VoteAttendance $voteAttendance)
    {
        if($request->ajax()){
            $student=Student::where('dni',$request->dni)->where('type','Estudiante')->first();
            if($student)
                $voteAttendance=$voteAttendance->store($request);
            if($voteAttendance && $student){
                return response()->json([
                    'cedula' => 'yes',
                    'message' => 'Elija a la Agrupación de su Preferencia',
                    'student' => $student,
                ],200);
            }elseif(!$student){
                return response()->json([
                    'cedula' => 'no',
                    'message' => 'El DNI no se encuentra registrado como estudiante',
                    'student' => "",
                ],200);
            } else{
                return response()->json([
                    'cedula' => 'no',
                    'message' => 'Ya has Votado',
                    'student' => $student,
                ],200);
            }
        }

        return redirect('home');
    }

    /**
     * Mostrar una asistencia a votacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\VoteAttendance  $voteAttendance
     * @return \Illuminate\Http\Response: Json con la asistencia encontrada
     */
    public function show(Request $request,VoteAttendance $voteAttendance)
    {
        if($request->ajax()){
            return response()->json([
                'voteAttendance' => $voteAttendance->load('student','period')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una asistencia a votacion
     *
     * @param  \App\Http\Requests\Election\VoteAttendace\StoreRequest  $request: request para modificado
     * @param  \App\Models\Election\VoteAttendance  $voteAttendance
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y asistencia modificada
     */
    public function update(StoreRequest $request, VoteAttendance $voteAttendance)
    {
        if($request->ajax()){
            $voteAttendance=$voteAttendance->my_update($request,$voteAttendance);
            return response()->json([
                'message' => 'Asistencia actualizada correctamente.',
                'voteAttendance' => $voteAttendance
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar una asistencia de votacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\VoteAttendance  $voteAttendance
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,VoteAttendance $voteAttendance)
    {
        if($request->ajax()){
            $voteAttendance = $voteAttendance->remove();
            return response()->json([
                'message' => 'Asistencia eliminada correctamente.',
                'voteAttendance' => $voteAttendance
            ],200);
        }

        return redirect('home');
    }
}
