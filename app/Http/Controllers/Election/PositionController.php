<?php

namespace App\Http\Controllers\Election;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Election\Position\StoreRequest;
use App\Http\Requests\Election\Position\UpdateRequest;
use App\Models\Election\Position;

class PositionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Position::class, 'position');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar cargos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de cargos
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $positions=Position::orderBy('id','DESC')
                                ->name($request->find)
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($positions,200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar un cargo
     *
     * @param  \App\Http\Requests\Election\Position\StoreRequest  $request: request para guardado
     * @param  \App\Models\Election\Position  $pemission
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y cargo guardado
     */
    public function store(StoreRequest $request,Position $position)
    {
        if($request->ajax()){
            $position= $position->create($request->all());
            return response()->json([
                'message' => 'Cargo creado correctamente.',
                'position' => $position
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un cargo
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\Position  $position
     * @return \Illuminate\Http\Response: Json con el cargo encontrado
     */
    public function show(Request $request,Position $position)
    {
        if($request->ajax()){
            return response()->json([
                'position' => $position
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un cargo
     *
     * @param  \App\Http\Requests\Election\Position\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Election\Position  $position
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el cargo modificado
     */
    public function update(UpdateRequest $request, Position $position)
    {
        if($request->ajax()){
            $position=$position->update($request->all());
            return response()->json([
                'message' => 'Cargo actualizado correctamente.',
                'position' => $position
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un cargo
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\Position  $position
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Position $position)
    {
        if($request->ajax()){
            $position=$position->delete();
            return response()->json([
                'message' => 'Cargo eliminado correctamente.',
                'position' => $position
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Listar todos los cargos dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de cargos
     */
    public function list(Request $request)
    {
        $this->authorize('viewAny', Position::class);
        if($request->ajax()){

            if($request->quantity)
                $positions = Position::orderBy('id','DESC')->name($request->find)->take($request->quantity)->get();
            else
                $positions = Position::orderBy('id','DESC')->name($request->find)->get();

            return  response()->json($positions,200);
        }
        
        return redirect('home');
    }
}
