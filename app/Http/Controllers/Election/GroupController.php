<?php

namespace App\Http\Controllers\Election;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Election\Group\StoreRequest;
use App\Http\Requests\Election\Group\UpdateRequest;
use App\Models\Election\Group;
use App\Models\Management\Period;
use PDF;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Group::class, 'group');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar agrupaciones
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de agrupaciones
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $groups=Group::with('candidates.student','candidates.position','period')
                                ->orderBy('id','DESC')
                                ->name($request->find)
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($groups,200);
        }

        return redirect('home');
    }

    /**
     * Guardar una agrupacion
     *
     * @param  \App\Http\Requests\Election\Group\StoreRequest  $request: request para guardado
     * @param  \App\Models\Election\Group  $group
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion, agrupcion y candidatos guardado
     */
    public function store(StoreRequest $request,Group $group)
    {
        if($request->ajax()){
            $group=$group->store($request);
            return response()->json([
                'message' => 'Agrupación creada correctamente.',
                'group' => $group->load('candidates.student','candidates.position','period'),
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un agrupacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\Group  $group
     * @return \Illuminate\Http\Response: Json con la agrupacion encontrado
     */
    public function show(Request $request,Group $group)
    {
        if($request->ajax()){
            return response()->json([
                'group' => $group->load('candidates')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una agrupacion
     *
     * @param  \App\Http\Requests\Election\Group\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Election\Group  $group
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion, agrupacion y candidatos modificado
     */
    public function update(UpdateRequest $request, Group $group)
    {
        if($request->ajax()){
            $group=$group->my_update($request,$group);
            return response()->json([
                'message' => 'Agrupación actualizada correctamente.',
                'group' => $group
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar una agrupacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\Group  $group
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Group $group)
    {
        if($request->ajax()){
            $group = $group->remove();
            return response()->json([
                'message' => 'Agrupación eliminada correctamente.',
                'group' => $group
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Guardar foto de la agrupacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\Group  $group
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la agrupacion, sus candidatos y su imagen actualizada 
     */
    public function updatePhoto(Request $request,Group $group)
    {
        $this->authorize('updatePhoto', $group);
        if($request->ajax()){
            if($request->photo){
                $group->picture = $group->savePicture($request->photo,$group->picture);
                $group->save();
                return response()->json([
                    'message' => 'Imagen actualizada correctamente.',
                    'group' => $group->load('candidates'),
                    'picture' => $group->picture
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                    'group' => $group->load('candidates')
                ],500);
            }
        }

        return redirect('home');
        
    }

    /**
     * Listar todas las agrupaciones dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de agrupaciones
     */
    public function list(Request $request)
    {
        if($request->ajax()){

            if($request->quantity)
                $groups=Group::where('period_id',Period::getCurrent()->id)->with('candidates.student','candidates.position','period')
                                ->orderBy('id','DESC')
                                ->name($request->find)
                                ->take($request->quantity)->get();
            else
               $groups=Group::where('period_id',Period::getCurrent()->id)->with('candidates.student','candidates.position','period')
                                ->orderBy('id','DESC')
                                ->name($request->find)
                                ->get();

            return  response()->json($groups,200);
        }
        
        return redirect('home');
    }

    /*
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Election\Group  $group
     * @return pdf vista de los candidatos
     */
    public function print(Request $request,Group $group)
    {
        $this->authorize('print', $group);
        $group->load('candidates.student','candidates.position','period');
        //return view('pages.report.candidates',compact('group'));
        $pdf = PDF::loadView('pages.report.candidates',compact('group'))->setPaper('a4','landscape');
        return $pdf->stream();
    }
}
