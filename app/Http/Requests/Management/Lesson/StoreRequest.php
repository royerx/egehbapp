<?php

namespace App\Http\Requests\Management\Lesson;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade' => array("required","max:1"),
            'section' => array("nullable","max:1"),
            'period'=>array("required")
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'grade.required' => 'Debe de ingresar el grado',
            'grade.max' => 'No debe de superar 1 caracter',
            'section.required' => 'Debe de ingresar la sección',
            'section.max' => 'No debe de superar 1 caracter',
            'period.required' => 'Debe de seleccionar un periodo'
        ];
    }
}
