<?php

namespace App\Http\Requests\Management\Period;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class UpdateRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","unique:periods,name,".$this->route('period')->id,"max:100"),
            'year' => array("required","digits:4","unique:periods,year,".$this->route('period')->id),
            'description' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:255") ,
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'name.required' => 'Debe de ingresar el nombre',
            'name.regex' => 'El formato del texto es inválido',
            'name.unique' => 'El nombre ya se encuentra registrado',
            'name.max' => 'El nombre no debe de superar los 100 caracteres',
            'year.required' => 'Debe de ingresar el año',
            'year.digits' =>'Solo se aceptan 4 dígitos',
            'year.unique' => 'El año ya se encuentra registrado',
            'description.regex' => 'El formato del texto es inválido',
            'description.max' => 'La descripción no debe de superar los 255 caracteres',
        ];
    }
}
