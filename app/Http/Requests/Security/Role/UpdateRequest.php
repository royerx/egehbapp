<?php

namespace App\Http\Requests\Security\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class UpdateRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#,._-]*)*)+$/","unique:roles,name,".$this->route('role')->id,"max:30"),
            'description' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#,._-]*)*)+$/","max:255"),
            'special' => array("nullable",Rule::in(['nothing', 'all-access','no-access'])),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'name.required' => 'Debe de ingresar el nombre del rol',
            'name.regex' => 'El formato del texto es inválido',
            'name.unique' => 'El nombre ya se encuentra registrado',
            'name.max' => 'El nombre no debe de superar los 30 caracteres',
            'description.required' => 'Debe de ingresar la descripción del rol',
            'description.regex' => 'El formato del texto es inválido',
            'description.max' => 'La descripción no debe de superar los 255 caracteres',
            'special.in' => 'El permiso solo debe ser ninguno,all-access o no-access'
        ];
    }
}
