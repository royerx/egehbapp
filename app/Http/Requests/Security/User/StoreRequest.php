<?php

namespace App\Http\Requests\Security\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => array("required","unique:users","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#,._-]*)*)+$/","max:50"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'login.required' => 'Debe de ingresar el usuario',
            'login.unique' => 'El usuario ya se encuentra registrado',
            'login.regex' => 'El formato de texto es inválido',
            'login.max' => 'El usuario no debe de superar los 50 caracteres',
        ];
    }
}
