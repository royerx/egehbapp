<?php

namespace App\Http\Requests\Security\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class UpdateRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            /*'lastName' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:50"),
            'firstName' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:50"),
            'dni' => array("required","digits_between:8,20","unique:people,dni,".$this->route('administrative')->id),
            'birthday' => array("required","date"),
            'gender' => array("required",Rule::in(['Hombre', 'Mujer'])),
            'address' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:100"),
            'phone' => array("nullable","digits_between:6,15"),
            'photo' => array("nullable","image"),
            'email' => array("required","email","max:255","unique:people,email,".$this->route('administrative')->id),*/
            'login' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#,._-]*)*)+$/","max:50","unique:users,login,".$this->route('user')->id),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'login.required' => 'Debe de ingresar el usuario',
            'login.unique' => 'El usuario ya se encuentra registrado',
            'login.regex' => 'El formato de texto es inválido',
            'login.max' => 'El usuario no debe de superar los 50 caracteres',
        ];
    }
}
