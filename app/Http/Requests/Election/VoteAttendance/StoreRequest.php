<?php

namespace App\Http\Requests\Election\VoteAttendance;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dni' => array("required","digits:8"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'dni.required' => 'Debe de ingresar el DNI',
            'dni.digits' => 'El DNI debe de tener 8 dígitos',
            
        ];
    }
}
