<?php

namespace App\Http\Requests\Election\Group;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#,._-]*)*)+$/","max:50"),
            'description' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#,._-]*)*)+$/","max:255") ,
            'photo' => array("required","image"),
            'period'=>array("required")
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'name.required' => 'Debe de ingresar el nombre',
            'name.regex' => 'El formato del texto es inválido',
            'name.max' => 'El nombre no debe de superar los 50 caracteres',
            'description.regex' => 'El formato del texto es inválido',
            'description.max' => 'El nombre no debe de superar los 255 caracteres',
            'photo.required' => 'Debe de seleccionar una imagen',
            'photo.image' =>'El archivo debe de ser imagen',
            'period.required' => 'Debe de seleccionar un periodo'
        ];
    }
}
