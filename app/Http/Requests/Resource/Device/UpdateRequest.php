<?php

namespace App\Http\Requests\Resource\Device;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class UpdateRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:15","unique:devices,code,".$this->route('device')->id),
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:255"),
            'brand' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:255"),
            'model' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:255"),
            'type' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:50"),
            'color' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:255"),
            'serie' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:255"),
            'state' => array("required",Rule::in(['Bueno', 'Regular','Malo'])),
            'date' => array("required",'date'),
            'amount' => array("required","regex:/^[\d]{0,16}(\.[\d]{1,2})?$/"),
            'place' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:255"),
            'detail' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:255"),
            'photo' => array("nullable","image")
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            //validación codigo
            'code.required' => 'Debe de ingresar el código',
            'code.regex' => 'El formato del texto es inválido',
            'code.unique' => 'El código ya se encuentra registrado', 
            'code.max' => 'El código no debe de superar los 15 caracteres',
            //validacion nombre
            'name.required' => 'Debe de ingresar el nombre',
            'name.regex' => 'El formato del texto es inválido',
            'name.max' => 'El nombre no debe de superar los 255 caracteres',
            //validacion marca
            'brand.regex' => 'El formato del texto es inválido',
            'brand.max' => 'La marca no debe de superar los 255 caracteres',
             //validacion modelo
            'model.regex' => 'El formato del texto es inválido',
            'model.max' => 'La marca no debe de superar los 255 caracteres',
            //validacion tipo de bien
            'type.required' => 'Debe de ingresar el tipo de bien',
            'type.regex' => 'El formato del texto es inválido',
            'type.max' => 'El nombre no debe de superar los 50 caracteres',
            //validacion color
            'color.regex' => 'El formato del texto es inválido',
            'color.max' => 'La marca no debe de superar los 255 caracteres',
            //validacion serie
            'serie.regex' => 'El formato del texto es inválido',
            'serie.max' => 'La marca no debe de superar los 255 caracteres',
            //validacion estado
            'state.required' => 'Debe de ingresar el estado',
            'state.in' => 'Elemento no permitido',
            //validacion fecha de ingreso
            'date.required' => 'Debe de ingresar la fecha de ingreso',
            'date.date' => 'Formato de fecha incorrecto',
            //validacion monto
            'amount.required' => 'Debe de ingresar el monto',
            'amount.regex' => 'El formato no es válido',
            //validacion ubicacion fisica
            'place.regex' => 'El formato del texto es inválido',
            'place.max' => 'La marca no debe de superar los 255 caracteres',
            //validacion observacion
            'detail.regex' => 'El formato del texto es inválido',
            'detail.max' => 'La marca no debe de superar los 255 caracteres',
            //validacion foto
            'photo.image' =>'El archivo debe de ser imagen',
        ];
    }
}
