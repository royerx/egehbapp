<?php

namespace App\Http\Requests\System\Parameter;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:100"),
            'value' => array("required","regex:/^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$/"),
            'beginDate' => array("required","date"),
            'endDate' => array("required","date"),
            'description' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#&%,._-]*)*)+$/","max:255") ,
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'name.required' => 'Debe de ingresar el nombre',
            'name.regex' => 'El formato del texto es inválido',
            'name.unique' => 'El nombre ya se encuentra registrado',
            'name.max' => 'El nombre no debe de superar los 100 caracteres',
            'value.required' => 'Debe de ingresar el valor',
            'value.regex' => 'Formato no válido',
            'beginDate.required' => 'Debe de ingresar la fecha',
            'beginDate.date' => 'Debe de ingresar una fecha válida',
            'endDate.required' => 'Debe de ingresar la fecha',
            'endDate.date' => 'Debe de ingresar una fecha válida',
            'description.regex' => 'El formato del texto es inválido',
            'description.max' => 'La descripción no debe de superar los 255 caracteres',
        ];
    }
}
