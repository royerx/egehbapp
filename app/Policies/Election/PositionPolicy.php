<?php

namespace App\Policies\Election;

use App\Models\Election\Position;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PositionPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Position
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('positions.list');
    }

    /**
     * Permiso para el metodo show para el modelo Position
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Position  $position
     * @return mixed
     */
    public function view(User $user, Position $position)
    {
        return $user->hasPermission('positions.show');
    }

    /**
     * Permiso para el metodo create para el modelo Position
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('positions.create');
    }

    /**
     * Permiso para el metodo update para el modelo Position
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Position  $position
     * @return mixed
     */
    public function update(User $user, Position $position)
    {
        return $user->hasPermission('positions.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Position
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Position  $position
     * @return mixed
     */
    public function delete(User $user, Position $position)
    {
        return $user->hasPermission('positions.delete');
    }
}
