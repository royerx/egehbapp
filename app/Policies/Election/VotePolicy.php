<?php

namespace App\Policies\Election;

use App\Models\Security\User;
use App\Models\Vote;
use Illuminate\Auth\Access\HandlesAuthorization;

class VotePolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Vote
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('votes.list');
    }

    /**
     * Permiso para el metodo show para el modelo Vote
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Vote  $vote
     * @return mixed
     */
    public function view(User $user, Vote $vote)
    {
        return $user->hasPermission('votes.show');
    }

    /**
     * Permiso para el metodo create para el modelo Vote
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('votes.create');
    }

    /**
     * Permiso para el metodo update para el modelo Vote
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Vote  $vote
     * @return mixed
     */
    public function update(User $user, Vote $vote)
    {
        return $user->hasPermission('votes.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Vote
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Vote  $vote
     * @return mixed
     */
    public function delete(User $user, vote $vote)
    {
        return $user->hasPermission('votes.delete');
    }
}
