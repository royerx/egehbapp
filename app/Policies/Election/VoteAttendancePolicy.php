<?php

namespace App\Policies\Election;

use App\Models\Security\User;
use App\Models\Election\VoteAttendance;
use Illuminate\Auth\Access\HandlesAuthorization;

class VoteAttendancePolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo VoteAttendance
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('voteAttendances.list');
    }

    /**
     * Permiso para el metodo show para el modelo VoteAttendance
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\VoteAttendance  $voteAttendance
     * @return mixed
     */
    public function view(User $user, VoteAttendance $voteAttendance)
    {
        return $user->hasPermission('voteAttendances.show');
    }

    /**
     * Permiso para el metodo create para el modelo VoteAttendance
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('voteAttendances.create');
    }

    /**
     * Permiso para el metodo update para el modelo VoteAttendance
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\VoteAttendance  $voteAttendance
     * @return mixed
     */
    public function update(User $user, VoteAttendance $voteAttendance)
    {
        return $user->hasPermission('voteAttendances.update');
    }

    /**
     * Permiso para el metodo delete para el modelo VoteAttendance
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\VoteAttendance  $voteAttendance
     * @return mixed
     */
    public function delete(User $user, voteAttendance $voteAttendance)
    {
        return $user->hasPermission('voteAttendances.delete');
    }
}
