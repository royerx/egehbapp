<?php

namespace App\Policies\Election;

use App\Models\Election\Candidate;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CandidatePolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo create para el modelo Candidate
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('groups.savecandidate');
    }

    /**
     * Permiso para el metodo delete para el modelo Candidate
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Candidate  $candidate
     * @return mixed
     */
    public function delete(User $user, Candidate $candidate)
    {
        return $user->hasPermission('groups.deletecandidate');
    }
}
