<?php

namespace App\Policies\Election;

use App\Models\Election\Group;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Group
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('groups.list');
    }

    /**
     * Permiso para el metodo show para el modelo Group
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Group  $group
     * @return mixed
     */
    public function view(User $user, Group $group)
    {
        return $user->hasPermission('groups.show');
    }

    /**
     * Permiso para el metodo create para el modelo Group
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('groups.create');
    }

    /**
     * Permiso para el metodo update para el modelo Group
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Group  $group
     * @return mixed
     */
    public function update(User $user, Group $group)
    {
        return $user->hasPermission('groups.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Group
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Group  $group
     * @return mixed
     */
    public function delete(User $user, Group $group)
    {
        return $user->hasPermission('groups.delete');
    }

    /**
     * Permiso para el metodo updatePhoto para el modelo Group
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Group  $group
     * @return boolean
     */
    public function updatePhoto(User $user, Group $group)
    {
        return $user->hasPermission('groups.picture');
    }

    /**
     * Permiso para el metodo print para el modelo Group
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Election\Group  $group
     * @return boolean
     */
    public function print(User $user, Group $group)
    {
        return $user->hasPermission('groups.print');
    }
}
