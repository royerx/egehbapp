<?php

namespace App\Policies\System;

use App\Models\Security\User;
use App\Models\System\Parameter;
use Illuminate\Auth\Access\HandlesAuthorization;

class ParameterPolicy
{
    use HandlesAuthorization;

        /**
     * Permiso para el metodo index para el modelo Parameter
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('parameters.list');
    }

    /**
     * Permiso para el metodo show para el modelo Parameter
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Parameter  $parameter
     * @return mixed
     */
    public function view(User $user, Parameter $parameter)
    {
        return $user->hasPermission('parameters.show');
    }

    /**
     * Permiso para el metodo create para el modelo Parameter
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('parameters.create');
    }

    /**
     * Permiso para el metodo update para el modelo Parameter
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Parameter  $parameter
     * @return mixed
     */
    public function update(User $user, Parameter $parameter)
    {
        return $user->hasPermission('parameters.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Parameter
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Parameter  $parameter
     * @return mixed
     */
    public function delete(User $user, Parameter $parameter)
    {
        return $user->hasPermission('parameters.delete');
    }
}
