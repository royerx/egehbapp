<?php

namespace App\Policies\Security;

use App\Models\Security\Role;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Role
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('roles.list');
    }

    /**
     * Permiso para el metodo show para el modelo Role
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Role  $role
     * @return boolean
     */
    public function view(User $user, Role $role)
    {
        return $user->hasPermission('roles.show');
    }

    /**
     * Permiso para el metodo create para el modelo Role
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('roles.create');
    }

    /**
     * Permiso para el metodo update para el modelo Role
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Role  $role
     * @return boolean
     */
    public function update(User $user, Role $role)
    {
        return $user->hasPermission('roles.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Role
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Role  $role
     * @return boolean
     */
    public function delete(User $user, Role $role)
    {
        return $user->hasPermission('roles.delete');
    }

    /**
     * Permiso para el metodo savePermissions para el modelo Role
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Role  $role
     * @return boolean
     */
    public function savePermissions(User $user, Role $role)
    {
        return $user->hasPermission('roles.permissions');
    }

    /**
     * Permiso para el metodo list para el modelo Role
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function list(User $user)
    {
        return $user->hasPermission('roles.list');
    }
}
