<?php

namespace App\Policies\Security;

use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo User
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('users.list');
    }

    /**
     * Permiso para el metodo show para el modelo User
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\User  $model
     * @return boolean
     */
    public function view(User $user, User $model)
    {
        return $user->hasPermission('users.show');
    }

    /**
     * Permiso para el metodo create para el modelo User
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('users.create');
    }

    /**
     * Permiso para metodo update para el modelo User
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\User  $model
     * @return boolean
     */
    public function update(User $user, User $model)
    {
        return $user->hasPermission('users.update');
    }

    /**
     * Permiso para el metodo delete para el modelo User
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\User  $model
     * @return boolean
     */
    public function delete(User $user, User $model)
    {
        return $user->hasPermission('users.delete');
    }

    /**
     * Permiso para el metodo updatePhoto para el modelo User
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\User  $model
     * @return boolean
     */
    public function updatePhoto(User $user, User $model)
    {
        return $user->hasPermission('users.picture');
    }

    /**
     * Permiso para el metodo saveRoles para el modelo User
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\User  $model
     * @return boolean
     */
    public function saveRoles(User $user, User $model)
    {
        return $user->hasPermission('users.role');
    }

    /**
     * Permiso para el metodo list para el modelo User
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function list(User $user)
    {
        return $user->hasPermission('users.list');
    }
}
