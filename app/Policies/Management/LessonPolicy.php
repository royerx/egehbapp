<?php

namespace App\Policies\Management;

use App\Models\Management\Lesson;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LessonPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Lesson
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('lessons.list');
    }

    /**
     * Permiso para el metodo show para el modelo Lesson
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Lesson  $lesson
     * @return mixed
     */
    public function view(User $user, Lesson $lesson)
    {
        return $user->hasPermission('lessons.show');
    }

    /**
     * Permiso para el metodo create para el modelo Lesson
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('lessons.create');
    }

    /**
     * Permiso para el metodo update para el modelo Lesson
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Lesson  $lesson
     * @return mixed
     */
    public function update(User $user, Lesson $lesson)
    {
        return $user->hasPermission('lessons.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Lesson
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Lesson  $lesson
     * @return mixed
     */
    public function delete(User $user, Lesson $lesson)
    {
        return $user->hasPermission('lessons.delete');
    }
}
