<?php

namespace App\Policies\Management;

use App\Models\Management\Administrative;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdministrativePolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Administrative
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('administratives.list');
    }

    /**
     * Permiso para el metodo show para el modelo Administrative
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Administrative  $administrative
     * @return mixed
     */
    public function view(User $user, Administrative $administrative)
    {
        return $user->hasPermission('administratives.show');
    }

    /**
     * Permiso para el metodo create para el modelo Administrative
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('administratives.create');
    }

    /**
     * Permiso para metodo update para el modelo Administrative
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Administrative  $administrative
     * @return mixed
     */
    public function update(User $user, Administrative $administrative)
    {
        return $user->hasPermission('administratives.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Administrative
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Administrative  $administrative
     * @return mixed
     */
    public function delete(User $user, Administrative $administrative)
    {
        return $user->hasPermission('administratives.delete');
    }

    /**
     * Permiso para el metodo updatePhoto para el modelo Administrative
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Administrative  $administrative
     * @return boolean
     */
    public function updatePhoto(User $user, Administrative $administrative)
    {
        return $user->hasPermission('administratives.picture');
    }
}
