<?php

namespace App\Policies\Management;

use App\Models\Management\Student;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Student
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('students.list');
    }

    /**
     * Permiso para el metodo show para el modelo Student
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Student  $student
     * @return mixed
     */
    public function view(User $user, Student $student)
    {
        return $user->hasPermission('students.show');
    }

    /**
     * Permiso para el metodo create para el modelo Student
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('students.create');
    }

    /**
     * Permiso para el metodo update para el modelo Student
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Student  $student
     * @return mixed
     */
    public function update(User $user, Student $student)
    {
        return $user->hasPermission('students.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Student
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Student  $student
     * @return mixed
     */
    public function delete(User $user, Student $student)
    {
        return $user->hasPermission('students.delete');
    }

    /**
     * Permiso para el metodo updatePhoto para el modelo Student
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Student  $student
     * @return boolean
     */
    public function updatePhoto(User $user, Student $student)
    {
        return $user->hasPermission('students.picture');
    }

}
