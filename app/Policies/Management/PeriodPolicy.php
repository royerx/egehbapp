<?php

namespace App\Policies\Management;

use App\Models\Management\Period;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PeriodPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Period
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('periods.list');
    }

    /**
     * Permiso para el metodo show para el modelo Period
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Period  $period
     * @return mixed
     */
    public function view(User $user, Period $period)
    {
        return $user->hasPermission('periods.show');
    }

    /**
     * Permiso para el metodo create para el modelo Period
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('periods.create');
    }

    /**
     * Permiso para el metodo update para el modelo Period
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Period  $period
     * @return mixed
     */
    public function update(User $user, Period $period)
    {
        return $user->hasPermission('periods.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Period
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Period  $period
     * @return mixed
     */
    public function delete(User $user, Period $period)
    {
        return $user->hasPermission('periods.delete');
    }

}
