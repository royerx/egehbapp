<?php

namespace App\Policies\Management;

use App\Models\Management\Teacher;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeacherPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Teacher
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('teachers.list');
    }

    /**
     * Permiso para el metodo show para el modelo Teacher
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Teacher  $teacher
     * @return mixed
     */
    public function view(User $user, Teacher $teacher)
    {
        return $user->hasPermission('teachers.show');
    }

    /**
     * Permiso para el metodo create para el modelo Teacher
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('teachers.create');
    }

    /**
     * Permiso para metodo update para el modelo Teacher
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Teacher  $teacher
     * @return mixed
     */
    public function update(User $user, Teacher $teacher)
    {
        return $user->hasPermission('teachers.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Teacher
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Teacher  $teacher
     * @return mixed
     */
    public function delete(User $user, Teacher $teacher)
    {
        return $user->hasPermission('teachers.delete');
    }

    /**
     * Permiso para el metodo updatePhoto para el modelo Teacher
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Management\Teacher  $teacher
     * @return boolean
     */
    public function updatePhoto(User $user, Teacher $teacher)
    {
        return $user->hasPermission('teachers.picture');
    }
}
