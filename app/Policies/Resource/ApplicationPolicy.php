<?php

namespace App\Policies\Resource;

use App\Models\Resource\Application;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicationPolicy
{
    use HandlesAuthorization;

     /**
     * Permiso para el metodo index para el modelo Application
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('applications.list');
    }

    /**
     * Permiso para el metodo show para el modelo Application
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Resource\Application  $application
     * @return mixed
     */
    public function view(User $user, Application $application)
    {
        return $user->hasPermission('applications.show');
    }

    /**
     * Permiso para el metodo create para el modelo Application
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('applications.create');
    }

    /**
     * Permiso para metodo update para el modelo Application
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Resource\Application  $application
     * @return mixed
     */
    public function update(User $user, Application $application)
    {
        return $user->hasPermission('applications.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Application
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Resource\Application  $application
     * @return mixed
     */
    public function delete(User $user, Application $application)
    {
        return $user->hasPermission('applications.delete');
    }
}
