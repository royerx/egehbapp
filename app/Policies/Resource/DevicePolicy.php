<?php

namespace App\Policies\Resource;

use App\Models\Resource\Device;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DevicePolicy
{
    use HandlesAuthorization;

     /**
     * Permiso para el metodo index para el modelo Device
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('devices.list');
    }

    /**
     * Permiso para el metodo show para el modelo Device
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Resource\Device  $device
     * @return mixed
     */
    public function view(User $user, Device $device)
    {
        return $user->hasPermission('devices.show');
    }

    /**
     * Permiso para el metodo create para el modelo Device
     *
     * @param  \App\Models\Security\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('devices.create');
    }

    /**
     * Permiso para metodo update para el modelo Device
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Resource\Device  $device
     * @return mixed
     */
    public function update(User $user, Device $device)
    {
        return $user->hasPermission('devices.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Device
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Resource\Device  $device
     * @return mixed
     */
    public function delete(User $user, Device $device)
    {
        return $user->hasPermission('devices.delete');
    }

    /**
     * Permiso para el metodo updatePhoto para el modelo Device
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Resource\Device  $device
     * @return boolean
     */
    public function updatePhoto(User $user, Device $device)
    {
        return $user->hasPermission('devices.picture');
    }

    /**
     * Permiso para el metodo updatePhoto para el modelo Device
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Resource\Device  $device
     * @return boolean
     */
    public function getByCode(User $user)
    {
        return $user->hasPermission('devices.code');
    }
}
