<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.includes.head')
</head>

<body>
    @yield('content')
    @include('layouts.includes.scripts')
</body>

</html>
