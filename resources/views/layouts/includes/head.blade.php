<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<meta name="description" content="Aplicacion base para procesos administrativos de la IE Heroes de la Breña">
<meta name="author" content="Royer Rodriguez">

<link rel="icon" href="{{ asset('img/assets/favicon.ico') }}">

@yield('head')