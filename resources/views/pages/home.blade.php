@extends('layouts.app')

@section('head')
    <title>Home</title>
@endsection

@section('content')
    <div id="app">
        <app />
    </div>
@endsection
