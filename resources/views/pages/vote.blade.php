<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.includes.head')
    <title>Elecciones Estudiantiles</title>
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>

<body class="full-cover-background">
    <div id="app">
        <vote/>
    </div>
    @include('layouts.includes.scripts')
</body>

</html>