@extends('layouts.app')

@section('head')
    <title>Login</title>
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="login-box">
        <img src="img/assets/logo.png" class="avatar" alt="Avatar Image">

        <h1>Inicio de Sesion</h1>

        <form method="POST" action="{{ route('login') }}">

            @csrf

            <!-- USERNAME INPUT -->
            <div class="login-control">
                <input class="form-control @error('login') is-invalid @enderror" type="text" name="login"
                    value="{{ old('login') }}" placeholder="Ingrese usuario" required>
                @error('login')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <!-- PASSWORD INPUT -->
            <div class="login-control">
                <input class="form-control @error('password') is-invalid @enderror" type="password" name="password"
                    value="{{ old('password') }}" placeholder="Ingrese contraseña" required>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <input type="submit">

            <div class="text-center">
                <a href="#">¿Olvidaste tu contraseña?</a><br>
                <a href="#">Registrarse</a>
            </div>
        </form>
    </div>
@endsection
