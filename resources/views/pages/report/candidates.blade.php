<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <style>
        body {
            font-family: "Nunito", sans-serif;
        }

        .main-title-elections {
            color: #fff;
            border-radius: 5px;
            background-color: rgba(0, 0, 0, 0.9);
            padding: 10px;
            margin: 0;
            /*top: 50%;
                transform: translate(-50%, -50%);
                -moz-transform: translate(-50%, -50%);
                -webkit-transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);*/
            width: 100%;
            height: 100px;
            display: inline-block;
        }

        .main-center {
            float: left;
            width: 80%;
        }

        .header-elections {
            color: #fff;
            border-radius: 5px;
            background-color: rgba(50, 64, 77, 0);
            position: absolute;
            margin: 1% 2%;
            /*top: 50%;
                transform: translate(-50%, -50%);
                -moz-transform: translate(-50%, -50%);
                -webkit-transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);*/
            width: 96%;
            text-align: center;
            font-weight: 900;
        }

        .main-left {
            float: left;
        }

        .main-right {
            float: right;
        }
        table { 
		    width: 100%; 
		    border-collapse: collapse; 
            color: black;
            font-family: "Nunito", sans-serif;
	    }
	/* Zebra striping */
	tr:nth-of-type(odd) { 
		background: #eee; 
	}
	th { 
		background: #333; 
		color: white; 
		font-weight: bold; 
	}
	td, th { 
		padding: 6px; */
		border: 1px 0px solid #ccc; 
		text-align: left; 
	}
    th{
        padding: 10px;
    }
    </style>
</head>

<body>
    <div class="header-elections">
        <div class="main-title-elections">
            <img src="{{asset('img/assets/jec.png')}}" width="100" alt="escudo" class="main-left" />
            <div class="main-center">
                <h2>CANDIDATOS DE LISTA {{$group->period->year}}</h2>
                <h4>{{$group->name}}</h4>
            </div>
            <img src="{{asset('img/assets/insignia.png')}}" width="60" alt="insignia" class="main-right" />
        </div>
        <br>
        <br>
        <div>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>
                                Apellidos y Nombres
                            </th>
                            <th width="80">
                                DNI
                            </th>
                            <th width="250">
                                Cargo
                            </th>
                            <th width="60">
                                Foto
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($group->candidates as $item )
                        <tr>
                            <td>{{ $item->student->fullName }}</td>
                            <td>{{ $item->student->dni }}</td>
                            <td>{{ $item->position->name }}</td>
                            <td>
                                <img width="50" src="{{asset(substr($item->student->picture,1))}}"/>
                            </td>
                            
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endForeach
                    </tbody>
                </table>
            </div>
</body>

</html>