<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <style>
        body {
            font-family: "Nunito", sans-serif;
        }

        .main-title-elections {
            color: #fff;
            border-radius: 5px;
            background-color: rgba(0, 0, 0, 0.9);
            padding: 10px;
            margin: 0;
            /*top: 50%;
                transform: translate(-50%, -50%);
                -moz-transform: translate(-50%, -50%);
                -webkit-transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);*/
            width: 100%;
            height: 100px;
            display: inline-block;
        }

        .main-center {
            float: left;
            width: 80%;
        }

        .header-elections {
            color: #fff;
            border-radius: 5px;
            background-color: rgba(50, 64, 77, 0);
            position: absolute;
            margin: 1% 2%;
            /*top: 50%;
                transform: translate(-50%, -50%);
                -moz-transform: translate(-50%, -50%);
                -webkit-transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);*/
            width: 96%;
            text-align: center;
            font-weight: 900;
        }

        .main-left {
            float: left;
        }

        .main-right {
            float: right;
        }
        table { 
		    width: 100%; 
		    border-collapse: collapse; 
            color: black;
            font-family: "Nunito", sans-serif;
	    }
	/* Zebra striping */
	tr:nth-of-type(odd) { 
		background: #eee; 
	}
	th { 
		background: #333; 
		color: white; 
		font-weight: bold; 
	}
	td, th { 
		padding: 6px; */
		border: 1px 0px solid #ccc; 
		text-align: left; 
	}
    th{
        padding: 10px;
    }
    </style>
</head>

<body>
    <div class="header-elections">
        <div class="main-title-elections">
            <img src="{{asset('img/assets/jec.png')}}" width="100" alt="escudo" class="main-left" />
            <div class="main-center">
                <h2>RESULTADOS ELECCIONES ESCOLARES</h2>
                <h4>INSTITUCIÓN EDUCATIVA HÉROES DE LA BREÑA {{$period->year}}</h4>
            </div>
            <img src="{{asset('img/assets/insignia.png')}}" width="60" alt="insignia" class="main-right" />
        </div>
        <br>
        <br>
        <div>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>
                                Agrupación Política
                            </th>
                            <th width="100">
                                Símbolo
                            </th>
                            <th width="80">
                                Votos
                            </th>
                            <th width="80">
                                Porcentaje
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($result['data'] as $item )
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>
                                <img width="80" src="{{asset($item->picture)}}"/>
                            </td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->percent }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endForeach
                    </tbody>
                </table>
                <br>
                <div class="main-right" width="50">
                    <table>
                    <tr>
                        <td>
                            <b>Total Estudiantes: </b><br />
                            <b>Votos Emitidos: </b><br />
                            <b>Participación: </b><br />
                            <b>Ausentismo: </b>
                        </td>
                        <td>
                            {{ $result['total'] }} <br />
                            {{ $result['quantity'] }} <br />
                            {{ $result['participation'] }} % <br />
                            {{ $result['absenteeism'] }} %
                        </td>
                    </tr>
                </table>
                </div>
            </div>
</body>

</html>