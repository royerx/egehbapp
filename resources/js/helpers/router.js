import Vue from 'vue'
import Router from 'vue-router'

/**
 * Configuracion de rutas
 * 
 * Permite configurar las rutas y sus respectivos componentes
 *
 * @export {router} instancia de vue-router
 */

Vue.use(Router)

const routes = [
    {
        path: '/mantenimiento/estudiantes',
        name: 'student',
        component: require('../views/pages/management/student/Index.vue').default
    },
    {
        path: '/mantenimiento/profesores',
        name: 'teacher',
        component: require('../views/pages/management/teacher/Index.vue').default
    },
    {
        path: '/mantenimiento/administrativos',
        name: 'administrative',
        component: require('../views/pages/management/administrative/Index.vue').default
    },
    {
        path: '/mantenimiento/periodos',
        name: 'period',
        component: require('../views/pages/management/period/Index.vue').default
    },
    {
        path: '/elecciones/agrupaciones',
        name: 'group',
        component: require('../views/pages/election/group/Index.vue').default
    },
    {
        path: '/elecciones/cargos',
        name: 'position',
        component: require('../views/pages/election/position/Index.vue').default
    },
    {
        path: '/resultados',
        name: 'result',
        component: require('../views/pages/election/result/Index.vue').default
    },
    {
        path: '/seguridad/roles',
        name: 'role',
        component: require('../views/pages/security/role/Index.vue').default
    },
    {
        path: '/seguridad/usuarios',
        name: 'user',
        component: require('../views/pages/security/user/Index.vue').default
    },
    {
        path: '/sistema/parametros',
        name: 'parameter',
        component: require('../views/pages/system/parameter/Index.vue').default
    },
    {
        path: '/recursos/equipos-mobiliario',
        name: 'device',
        component: require('../views/pages/resource/device/Index.vue').default
    },
    {
        path: '/recursos/pedido',
        name: 'application',
        component: require('../views/pages/resource/application/Index.vue').default
    },
    {
        path: '/asistencia',
        name: 'attendanceReport',
        component: require('../views/pages/report/Attendance.vue').default
    },
    {
        path: '/reporte-estudiante',
        name: 'studentReport',
        component: require('../views/pages/report/Student.vue').default
    },
    {
        path: '/cuaderno-incidencias',
        name: 'notebook',
        component: require('../views/pages/report/Notebook.vue').default
    },
    {
        path: '/solicitudes',
        name: 'applicationReport',
        component: require('../views/pages/report/Application.vue').default
    },
    {
        path: '/faltas',
        name: 'absenceReport',
        component: require('../views/pages/report/Absence.vue').default
    },
];

const router = new Router({
    mode: 'history',
    routes: routes
});

export default router