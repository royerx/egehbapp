import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

//Importacion de paquetes de idiomas de vuetify
import es from 'vuetify/es5/locale/es'
import en from 'vuetify/es5/locale/en'

//importacion de los iconos
import '@mdi/font/css/materialdesignicons.css'

/**
 * vuetify
 *
 * Carga la instancia de vuetify y se realiza la configuracion de idioma y los iconos
 *
 * @export {Vuetify} instancia de vuetify
 */

Vue.use(Vuetify)

const opts = {
    icons: {
        iconfont: 'mdiSvg'
    },
    lang: {
        locales: { es, en },
        current: 'es'
    },
}

export default new Vuetify(opts)