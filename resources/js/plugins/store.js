/**
 * Componente de Vuex
 */
export default {
    //strict:true,

    /** Variables globales */
    state: {
        user: {},
        role: {},
        currentPeriod:{},
        snackbar: {
            visible: false,
            color: "success",
            text: null,
            timeout: 5000
        },
        person:{
            STUDENT:'Estudiante',
            TEACHER: 'Profesor',
            ADMINISTRATIVE: 'Administrativo',
        },
        security: {

            //permiso para menu seguridad
            menuSecurity: 'menu.security',

            //permisos para submenu roles
            rolesList: 'roles.list',
            rolesCreate: 'roles.create',
            rolesUpdate: 'roles.update',
            rolesShow: 'roles.show',
            rolesDelete: 'roles.delete',
            rolesPermission: 'roles.permissions',

            //permisos para submenu usuarios
            usersList: 'users.list',
            usersCreate: 'users.create',
            usersUpdate: 'users.update',
            usersShow: 'users.show',
            usersDelete: 'users.delete',
            usersPicture: 'users.picture',
            usersRole: 'users.role',

            //permiso para menu mantenimiento
            menuSecurity: 'menu.management',

            //permisos para submenu estudiantes
            studentsList: 'students.list',
            studentsCreate: 'students.create',
            studentsUpdate: 'students.update',
            studentsShow: 'students.show',
            studentsDelete: 'students.delete',
            studentsPicture: 'students.picture',

            //permisos para submenu profesores
            teachersList: 'teachers.list',
            teachersCreate: 'teachers.create',
            teachersUpdate: 'teachers.update',
            teachersShow: 'teachers.show',
            teachersDelete: 'teachers.delete',
            teachersPicture: 'teachers.picture',

            //permisos para submenu administrativos
            administrativesList: 'administratives.list',
            administrativesCreate: 'administratives.create',
            administrativesUpdate: 'administratives.update',
            administrativesShow: 'administratives.show',
            administrativesDelete: 'administratives.delete',
            administrativesPicture: 'administratives.picture',

            //permisos para submenu periodos
            periodsList: 'periods.list',
            periodsCreate: 'periods.create',
            periodsUpdate: 'periods.update',
            periodsShow: 'periods.show',
            periodsDelete: 'periods.delete',

            //permiso para menu mantenimiento
            menuElection: 'menu.election',

            //permisos para submenu periodos
            positionsList: 'positions.list',
            positionsCreate: 'positions.create',
            positionsUpdate: 'positions.update',
            positionsShow: 'positions.show',
            positionsDelete: 'positions.delete',

            //permisos para submenu agrupaciones
            groupsList: 'groups.list',
            groupsCreate: 'groups.create',
            groupsUpdate: 'groups.update',
            groupsShow: 'groups.show',
            groupsDelete: 'groups.delete',
            groupsPicture: 'groups.picture',
            groupsPrint: 'groups.print',
            groupsSaveCandidate: 'groups.savecandidate',
            groupsDeleteCandidate: 'groups.deletecandidate',

            //permiso para menu recursos
            menuResource: 'menu.resource',

            //permisos para submenu Equipos y Moviliarios
            devicesList: 'devices.list',
            devicesCreate: 'devices.create',
            devicesUpdate: 'devices.update',
            devicesShow: 'devices.show',
            devicesDelete: 'devices.delete',
            devicesPicture: 'devices.picture',
            devicesCode: 'devices.code',

            //permisos para submenu solicitudes
            applicationsList: 'applications.list',
            applicationsCreate: 'applications.create',
            applicationsUpdate: 'applications.update',
            applicationsShow: 'applications.show',
            applicationsDelete: 'applications.delete',
            applicationsPicture: 'applications.picture',

            //permiso para menu recursos
            menuSystem: 'menu.system',

            //permisos para submenu Equipos y Moviliarios
            parametersList: 'parameters.list',
            parametersCreate: 'parameters.create',
            parametersUpdate: 'parameters.update',
            parametersShow: 'parameters.show',
            parametersDelete: 'parameters.delete',
        }
    },

    /** Metodos para obtener valores de variables */
    getters: {
        user: state => state.user,
        role: state => state.role,
        currentPeriod:state => state.currentPeriod,
        snackbar: state => state.snackbar,
        person: state => state.person,
        security: state=>state.security,
        can: state => permission =>{
            if (state.role.special == "all-access") {
                return true
            }

            if (state.role.special == "no-access") {
                return false
            }
            
            if (state.role.permission_actions) {
                for (let element of state.role.permission_actions) {
                    if (element.slug == permission) {
                        return true
                    }
                }
            }
            return false
        }
    },

    /** Metodos globales*/
    mutations: {
        /**
         * Mostrar el snackbar
         * 
         * Muetra los mensajes de confirmacion, validacion o error en un modal flotante
         * 
         * @param {Object} state instancia de variables globales
         * @return {object} playload parametros de configuracion del snackbar
         * 
         */
        showAlert(state, payload) {

            state.snackbar.text = payload.text;

            //color del snackbar
            if (payload.color) {
                state.snackbar.color = payload.color;
            }

            // Tiempo de duración del snakbar
            if (payload.timeout) {
                state.snackbar.timeout = payload.timeout;
            }

            state.snackbar.visible = true;
        },

        /**
         * Cerrar el snackbar
         * 
         * Cierra el modal flotante y reinicia los valores del modal
         * 
         * @param {Object} state instancia de variables globales
        */
        closeAlert(state) {
            state.snackbar.visible = false;
            state.snackbar.multiline = false;
            state.snackbar.text = null;
        },

        /**
         * Setear usuario logueado
         * 
         * Recibe un objeto con la informacion del usuario y se lo asigna al usuario logueado
         * 
         * @param {Object} state instancia de variables globales
         * @return {object} data datos del usuario logueado
        */
        setUser(state, data) {
            state.user = data
        },

        /**
         * Setear role seleccionado
         * 
         * Recibe un objeto con la informacion del rol seleccionado del usuario logueado
         * 
         * @param {Object} state instancia de variables globales
         * @return {object} data datos del rol seleccionado del usuario logueado
        */
        setRole(state, data) {
            state.role = data
        },

        setCurrentPeriod(state,data){
            state.currentPeriod=data
        },
    },
    actions: {

    }
}