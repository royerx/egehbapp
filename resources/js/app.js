/** referencia de lodash */
window._ = require('lodash');

/** referencia de axios */
window.axios = require('axios');

/** importacion de componentes */
import Vue from 'vue'
import Vuex from 'vuex'
import vuetify from './plugins/vuetify'
import storeData from './plugins/store'
import router from './helpers/router'
import { initialize } from './helpers/initialize'
import { ValidationProvider, ValidationObserver } from 'vee-validate'
import { extend } from 'vee-validate'
import * as rules from 'vee-validate/dist/rules'
import es from 'vee-validate/dist/locale/es.json';
import VueQrcodeReader from "vue-qrcode-reader";

Vue.prototype.$pagesVisible = 5;

/** Adicionar CRF Token a peticiones ajax */
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/** Inicializacion del store de vuex*/
Vue.use(Vuex)
const store = new Vuex.Store(storeData)
initialize(store)

/** Iniciaizacion de vue-moment */
Vue.use(require('vue-moment'));

/** Cargar reglas y componenetes de vee-validate */
Object.keys(rules).forEach(rule => {
    extend(rule, {
        ...rules[rule],
        message: es.messages[rule]
    })
})

/** Iniciaizacion de lector de QR */
Vue.use(VueQrcodeReader)

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

/** cargar componente de paginación */
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component('app', require('./views/layouts/App.vue').default)
Vue.component('vote', require('./views/pages/election/vote/Index.vue').default)

/**
 * instancia de vue
 */
const app = new Vue({
    vuetify,
    store,
    router,
    el: '#app',
});
