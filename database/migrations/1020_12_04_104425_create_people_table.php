<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['Administrativo', 'Estudiante','Profesor'])->nullable();
            $table->string('lastName', 100);
            $table->string('firstName', 100);
            $table->char('dni', 8)->unique();
            $table->date('birthday')->nullable();
            $table->enum('gender', ['Hombre', 'Mujer'])->nullable();
            $table->string('address')->nullable();
            $table->string('phone',15)->nullable();
            $table->string('picture')->nullable();
            $table->string('email')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
