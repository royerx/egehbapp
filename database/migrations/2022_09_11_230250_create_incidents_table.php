<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->time('time');
            $table->string('detail')->nullable();
            $table->unsignedBigInteger('student_id')->index();
            $table->foreign('student_id')->references('id')->on('people')->onDelete('restrict');
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('people')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidents');
    }
}
