<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Security\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id' => '1',
            'name' => 'Admin',
            'description' => 'Rol por defecto que contiene todos los permisos',
            'special' => 'all-access',
        ]);
    }
}
