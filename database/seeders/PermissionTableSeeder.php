<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Security\Permission;
use App\Models\Security\PermissionAction;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permisos para menu
        $permission = Permission::create([
                        'name' => 'Menu Principal',
                        'slug' => 'menu',
                        'description' => 'Permite mostrar los diferentes menus de los módulos',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Menu Seguridad',
                'slug' => 'menu.security',
                'description' => 'Mostrar el submenú de seguridad',
            ],
            [
                'name' => 'Menu Mantenimiento',
                'slug' => 'menu.management',
                'description' => 'Mostrar el submenú de mantenimiento',
            ],
        ]);

        //Permisos para modulo usuarios
        $permission = Permission::create([
                        'name' => 'Módulo Usuarios',
                        'slug' => 'users',
                        'description' => 'Permisos para el módulo de usuarios',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Usuarios',
                'slug' => 'users.list',
                'description' => 'Listar los usuarios registrados y mostrar el submenu Usuarios ',
            ],
            [
                'name' => 'Agregar Usuario',
                'slug' => 'users.create',
                'description' => 'Agregar un nuevo usuario',
            ],
            [
                'name' => 'Editar Usuario',
                'slug' => 'users.update',
                'description' => 'Editar un usuario en específico',
            ],
            [
                'name' => 'Mostrar Usuario',
                'slug' => 'users.show',
                'description' => 'Mostrar un usuario en específico',
            ],
            [
                'name' => 'Eliminar Usuario',
                'slug' => 'users.delete',
                'description' => 'Eliminar un usuario en específico',
            ],
            [
                'name' => 'Actualizar Foto de Usuario',
                'slug' => 'users.picture',
                'description' => 'Actualizar la foto de un usuario en específico',
            ],
            [
                'name' => 'Asignar Rol',
                'slug' => 'users.role',
                'description' => 'Asignar roles a un usuario',
            ],
        ]);

        //Permisos para modulo roles
        $permission = Permission::create([
                        'name' => 'Módulo Roles',
                        'slug' => 'roles',
                        'description' => 'Permisos para el módulo de roles',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Roles',
                'slug' => 'roles.list',
                'description' => 'Listar los roles registrados y mostrar el submenu Roles ',
            ],
            [
                'name' => 'Agregar Rol',
                'slug' => 'roles.create',
                'description' => 'Agregar un nuevo rol',
            ],
            [
                'name' => 'Editar Rol',
                'slug' => 'roles.update',
                'description' => 'Editar un rol en específico',
            ],
            [
                'name' => 'Mostrar Rol',
                'slug' => 'roles.show',
                'description' => 'Mostrar un rol en específico',
            ],
            [
                'name' => 'Eliminar Rol',
                'slug' => 'roles.delete',
                'description' => 'Eliminar un rol en específico',
            ],
            [
                'name' => 'Asignar Permisos',
                'slug' => 'roles.permissions',
                'description' => 'Asignar permisos a un rol',
            ],
        ]);

        //Permisos para modulo estudiantes
        $permission = Permission::create([
                        'name' => 'Módulo Estudiantes',
                        'slug' => 'students',
                        'description' => 'Permisos para el módulo de estudiantes',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Estudiantes',
                'slug' => 'students.list',
                'description' => 'Listar los estudiantes registrados y mostrar el submenu Estudiantes ',
            ],
            [
                'name' => 'Agregar Estudiante',
                'slug' => 'students.create',
                'description' => 'Agregar un nuevo estudiante',
            ],
            [
                'name' => 'Editar Estudiante',
                'slug' => 'students.update',
                'description' => 'Editar un estudiante en específico',
            ],
            [
                'name' => 'Mostrar Estudiante',
                'slug' => 'students.show',
                'description' => 'Mostrar un estudiante en específico',
            ],
            [
                'name' => 'Eliminar Estudiante',
                'slug' => 'students.delete',
                'description' => 'Eliminar un estudiante en específico',
            ],
            [
                'name' => 'Actualizar Foto de Estudiante',
                'slug' => 'students.picture',
                'description' => 'Actualizar la foto de un estudiante en específico',
            ],
        ]);

        //Permisos para modulo profesores
        $permission = Permission::create([
                        'name' => 'Módulo Profesores',
                        'slug' => 'teachers',
                        'description' => 'Permisos para el módulo de profesores',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Profesores',
                'slug' => 'teachers.list',
                'description' => 'Listar los profesores registrados y mostrar el submenu Profesores ',
            ],
            [
                'name' => 'Agregar Profesor',
                'slug' => 'teachers.create',
                'description' => 'Agregar un nuevo profesor',
            ],
            [
                'name' => 'Editar Profesor',
                'slug' => 'teachers.update',
                'description' => 'Editar un profesor en específico',
            ],
            [
                'name' => 'Mostrar Profesor',
                'slug' => 'teachers.show',
                'description' => 'Mostrar un profesor en específico',
            ],
            [
                'name' => 'Eliminar Profesor',
                'slug' => 'teachers.delete',
                'description' => 'Eliminar un profesor en específico',
            ],
            [
                'name' => 'Actualizar Foto de Profesor',
                'slug' => 'teachers.picture',
                'description' => 'Actualizar la foto de un profesor en específico',
            ],
        ]);

        //Permisos para modulo administrativos
        $permission = Permission::create([
                        'name' => 'Módulo Administrativos',
                        'slug' => 'administratives',
                        'description' => 'Permisos para el módulo de profesores',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Administrativos',
                'slug' => 'administratives.list',
                'description' => 'Listar los administrativos registrados y mostrar el submenu Administrativos ',
            ],
            [
                'name' => 'Agregar Administrativo',
                'slug' => 'administratives.create',
                'description' => 'Agregar un nuevo administrativo',
            ],
            [
                'name' => 'Editar Administrativo',
                'slug' => 'administratives.update',
                'description' => 'Editar un administrativo en específico',
            ],
            [
                'name' => 'Mostrar Administrativo',
                'slug' => 'administratives.show',
                'description' => 'Mostrar un administrativo en específico',
            ],
            [
                'name' => 'Eliminar Administrativo',
                'slug' => 'administratives.delete',
                'description' => 'Eliminar un administrativo en específico',
            ],
            [
                'name' => 'Actualizar Foto de Administrativo',
                'slug' => 'administratives.picture',
                'description' => 'Actualizar la foto de un administrativo en específico',
            ],
        ]);

        //Permisos para modulo periodos
        $permission = Permission::create([
                        'name' => 'Módulo Periodos Académicos',
                        'slug' => 'periods',
                        'description' => 'Permisos para el módulo de periodos académicos',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Periodos Académicos',
                'slug' => 'periods.list',
                'description' => 'Listar los periodos académicos registrados y mostrar el submenu Periodos ',
            ],
            [
                'name' => 'Agregar Periodo Académico',
                'slug' => 'periods.create',
                'description' => 'Agregar un nuevo periodo acadénico',
            ],
            [
                'name' => 'Editar Periodo Académico',
                'slug' => 'periods.update',
                'description' => 'Editar un periodo acadénico en específico',
            ],
            [
                'name' => 'Mostrar Periodo Académico',
                'slug' => 'periods.show',
                'description' => 'Mostrar un periodo acadénico en específico',
            ],
            [
                'name' => 'Eliminar Periodo Académico',
                'slug' => 'periods.delete',
                'description' => 'Eliminar un periodo acadénico en específico',
            ],
        ]);

        //Permisos para modulo cargos
        $permission = Permission::create([
                        'name' => 'Módulo Cargo',
                        'slug' => 'positions',
                        'description' => 'Permisos para el módulo de cargos',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Cargos',
                'slug' => 'positions.list',
                'description' => 'Listar los periodos académicos registrados y mostrar el submenu Periodos ',
            ],
            [
                'name' => 'Agregar Cargo',
                'slug' => 'positions.create',
                'description' => 'Agregar un nuevo cargo',
            ],
            [
                'name' => 'Editar Cargo',
                'slug' => 'positions.update',
                'description' => 'Editar un cargo en específico',
            ],
            [
                'name' => 'Mostrar Cargo',
                'slug' => 'positions.show',
                'description' => 'Mostrar un cargo en específico',
            ],
            [
                'name' => 'Eliminar Cargo',
                'slug' => 'positions.delete',
                'description' => 'Eliminar un cargo en específico',
            ],
        ]);

        //Permisos para modulo agrupaciones
        $permission = Permission::create([
                        'name' => 'Módulo Agrupaciones',
                        'slug' => 'groups',
                        'description' => 'Permisos para el módulo de agrupaciones',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Agrupaciones',
                'slug' => 'groups.list',
                'description' => 'Listar las agrupaciones registradas y mostrar el submenu Agrupaciones ',
            ],
            [
                'name' => 'Agregar Agrupación',
                'slug' => 'groups.create',
                'description' => 'Agregar una nueva agrupación',
            ],
            [
                'name' => 'Editar Agrupación',
                'slug' => 'groups.update',
                'description' => 'Editar una agrupación en específico',
            ],
            [
                'name' => 'Mostrar Agrupación',
                'slug' => 'groups.show',
                'description' => 'Mostrar una agrupación en específico',
            ],
            [
                'name' => 'Eliminar Agrupación',
                'slug' => 'groups.delete',
                'description' => 'Eliminar una agrupación en específico',
            ],
            [
                'name' => 'Actualizar Foto de Agrupación',
                'slug' => 'groups.picture',
                'description' => 'Actualizar la foto de una agrupación en específico',
            ],
            [
                'name' => 'Agregar Candidato',
                'slug' => 'groups.savecandidate',
                'description' => 'Agregar candidato a una agrupación en específico',
            ],
            [
                'name' => 'Eliminar Candidato',
                'slug' => 'groups.deletecandidate',
                'description' => 'Eliminar candidato de una agrupación en específico',
            ],
        ]);
        //Permisos para modulo elecciones estudianteiles
        $permission = Permission::create([
                        'name' => 'Módulo Elecciones Estudiantiles',
                        'slug' => 'elections',
                        'description' => 'Permisos para el módulo de agrupaciones',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Ver Cédula de Votación',
                'slug' => 'elections.vote',
                'description' => 'Mostrar submenú cédula de votación',
            ],
            [
                'name' => 'Ver Padrón Electoral',
                'slug' => 'elections.register',
                'description' => 'Mostrar el padrón electoral',
            ],
        ]);
    }
}
