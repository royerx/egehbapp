<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Security\User;
use App\Models\Management\Person;
use App\Models\Management\Teacher;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person = Person::create([
            'id' => '1',
            'lastName' => 'Admin',
            'firstName' => 'Super',
            'dni' => '00000000',
            'picture' => '/img/profiles/user.png',
            'email' => 'royerx.rodriguez@hotmail.com',
        ]);
        $user=User::create([
            'id' => '1',
            'login' => 'admin',
            'password' => bcrypt('admin123'),
            'person_id' => '1',
        ]);
        $user->roles()->attach(1);
    }
}
