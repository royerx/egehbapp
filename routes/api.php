<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthenticateController;
use App\Http\Controllers\API\AttendanceController;
use App\Http\Controllers\API\IncidentController;
use App\Http\Controllers\API\ApplicationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login',[AuthenticateController::class,'login'])->name('api.login');

Route::group(['middleware' => ['auth:sanctum']],function(){
    
    #rutas para salir de sesion
    Route::get('logout',[AuthenticateController::class,'logout'])->name('api.logout');
    
    #rutas para asistencia
    Route::get('student',[AttendanceController::class,'student'])->name('attendance.student');
    Route::post('attendance',[AttendanceController::class,'record'])->name('attendance.record');
    Route::get('students',[AttendanceController::class,'students'])->name('attendance.students');
    
    #rutas para justificaciones e incidentes
    Route::post('excuse',[AttendanceController::class,'excuse'])->name('attendance.excuse');
    Route::post('incident',[IncidentController::class,'store'])->name('incident.store');
    
    #rutas para pedidos
    Route::resource('application', ApplicationController::class)->except(['create', 'edit']);
    /*Route::post('application',[ApplicationController::class,'create'])->name('application.store');
    Route::put('application/{application}',[ApplicationController::class,'update'])->name('application.update');
    Route::delete('application/{application}',[ApplicationController::class,'destroy'])->name('application.delete');*/
    
});
