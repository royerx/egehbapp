<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Security\LoginController;
use App\Http\Controllers\Security\PermissionController;
use App\Http\Controllers\Security\RoleController;
use App\Http\Controllers\Security\UserController;
use App\Http\Controllers\Management\PersonController;
use App\Http\Controllers\Management\StudentController;
use App\Http\Controllers\Management\TeacherController;
use App\Http\Controllers\Management\AdministrativeController;
use App\Http\Controllers\Management\PeriodController;
use App\Http\Controllers\Management\LessonController;
use App\Http\Controllers\Election\GroupController;
use App\Http\Controllers\Election\PositionController;
use App\Http\Controllers\Election\CandidateController;
use App\Http\Controllers\Election\VoteAttendanceController;
use App\Http\Controllers\Election\VoteController;
use App\Http\Controllers\System\ParameterController;
use App\Http\Controllers\Resource\DeviceController;
use App\Http\Controllers\Resource\ApplicationController;
use App\Http\Controllers\Resource\ReportController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

#Rutas para el inicio de sesion
Route::get('login',[LoginController::class,'index'])->name('login');
Route::post('login',[LoginController::class,'login']);
Route::get('logout',[LoginController::class,'logout'])->name('logout');

#Rutas para la asignacion de rol y pagina de inicio: usa el middleware auth en el controlador
Route::get('/', [AdminController::class,'index']);
Route::get('home',[AdminController::class,'index'])->name('home');
Route::get('current',[AdminController::class,'currentUser'])->name('user.current');
Route::get('authroles',[AdminController::class,'userRoles'])->name('auth.roles');
Route::get('selectedrole',[AdminController::class,'selectedRole'])->name('role.selected');
Route::post('assign',[AdminController::class,'assignRole'])->name('role.assign');

Route::get('votacion',[LoginController::class,'vote'])->name('vote');
Route::post('voteattendance',[VoteAttendanceController::class,'store'])->name('voteattendance.store');
Route::get('groups',[GroupController::class,'list'])->name('group.list');
Route::post('vote',[VoteController::class,'store'])->name('vote.store');

Route::middleware(['auth'])->group(function(){

    Route::get('currentperiod', [AdminController::class,'getCurrentPeriod'])->name('currentperiod');

    #Rutas para el modulo de seguridad
    Route::get('permission',[PermissionController::class,'index'])->name('permission.index');

    Route::resource('role', RoleController::class)->except(['create', 'edit']);
    Route::get('roles',[RoleController::class,'list'])->name('role.list');
    Route::post('role/{role}/permission',[RoleController::class,'savePermissions'])->name('role.permission');

    Route::resource('user', UserController::class)->except(['create', 'edit']);
    Route::post('user/{user}/photo',[UserController::class,'updatePhoto'])->name('user.picture');
    Route::post('user/{user}/role',[UserController::class,'saveRoles'])->name('user.role');
    Route::get('person',[PersonController::class,'index'])->name('person.index');

    Route::resource('student', StudentController::class)->except(['create', 'edit']);
    Route::post('student/{student}/photo',[StudentController::class,'updatePhoto'])->name('student.picture');
    Route::get('students',[StudentController::class,'list'])->name('student.list');

    Route::resource('teacher', TeacherController::class)->except(['create', 'edit']);
    Route::post('teacher/{teacher}/photo',[TeacherController::class,'updatePhoto'])->name('teacher.picture');

    Route::resource('administrative', AdministrativeController::class)->except(['create', 'edit']);
    Route::post('administrative/{administrative}/photo',[AdministrativeController::class,'updatePhoto'])->name('administrative.picture');
    Route::get('teachers',[TeacherController::class,'list'])->name('teacher.list');

    Route::resource('period', PeriodController::class)->except(['create', 'edit']);
    Route::get('periods',[PeriodController::class,'list'])->name('period.list');
    Route::patch('period/{period}',[PeriodController::class,'setActive'])->name('period.setactive');

    Route::resource('parameter', ParameterController::class)->except(['create', 'edit']);

    Route::resource('lesson', LessonController::class)->except(['create', 'edit']);

    Route::resource('position', PositionController::class)->except(['create', 'edit']);
    Route::get('positions',[PositionController::class,'list'])->name('position.list');
    
    Route::resource('group', GroupController::class)->except(['create', 'edit']);
    Route::post('group/{group}/photo',[GroupController::class,'updatePhoto'])->name('group.picture');
    Route::get('elecciones/candidatos/{group}',[GroupController::class,'print'])->name('group.print');

    Route::resource('candidate', CandidateController::class)->only(['store', 'destroy']);

    Route::get('vote', [VoteController::class,'index'])->name('vote.index')->middleware('permission:elections.result');
    Route::get('result/{period}', [VoteController::class,'result'])->name('vote.result')->middleware('permission:elections.result');

    Route::resource('device', DeviceController::class)->except(['create', 'edit']);
    Route::post('device/{device}/photo',[DeviceController::class,'updatePhoto'])->name('device.picture');
    Route::get('devices',[DeviceController::class,'list'])->name('device.list');
    Route::get('device-find',[DeviceController::class,'names'])->name('device.find');
    Route::get('device-code',[DeviceController::class,'getByCode'])->name('device.code');

    Route::resource('application', ApplicationController::class)->except(['create', 'edit']);

    Route::get('attendance-report',[ReportController::class,'attendance'])->name('report.attendance')->middleware('permission:reports.attendance');
    Route::get('attendance-report/{student}',[ReportController::class,'student'])->name('report.attendance')->middleware('permission:reports.student');
    Route::get('incidents-report',[ReportController::class,'notebook'])->name('report.notebook')->middleware('permission:reports.notebook');
    Route::get('application-report',[ReportController::class,'application'])->name('report.application')->middleware('permission:reports.application');
    Route::get('absence-report',[ReportController::class,'absences'])->name('report.absence')->middleware('permission:reports.absence');
});

Route::fallback(function () {
    return redirect()->route('home');
});